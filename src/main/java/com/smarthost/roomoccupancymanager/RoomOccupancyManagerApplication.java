/*
 * Copyright (c) 2021 Fadi William Ghali Abdelmessih<fadi.william.ghali@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies
 * or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

package com.smarthost.roomoccupancymanager;

import com.smarthost.roomoccupancymanager.config.ApplicationProperties;
import com.smarthost.roomoccupancymanager.config.profile.Constants;
import com.smarthost.roomoccupancymanager.config.profile.DefaultProfileUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.core.env.Environment;

import javax.annotation.PostConstruct;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.Collection;

/**
 * Application Entry Point.
 *
 * @author Fadi William Ghali Abdelmessih
 */
@SpringBootApplication
@EnableConfigurationProperties({ ApplicationProperties.class })
public class RoomOccupancyManagerApplication {

  private static final Logger log = LoggerFactory.getLogger(
    RoomOccupancyManagerApplication.class
  );

  private final Environment env;

  public RoomOccupancyManagerApplication(Environment env) {
    this.env = env;
  }

  @PostConstruct
  public void initApplication() {
    Collection<String> activeProfiles = Arrays.asList(env.getActiveProfiles());
    if (
      activeProfiles.contains(Constants.SPRING_PROFILE_DEVELOPMENT) &&
      activeProfiles.contains(Constants.SPRING_PROFILE_PRODUCTION)
    ) {
      log.error(
        "You have misconfigured your application! It should not run " +
        "with both the 'dev' and 'prod' profiles at the same time."
      );
    }
  }

  public static void main(String[] args) {
    // Run our Spring Boot Application.
    SpringApplication app = new SpringApplication(
      RoomOccupancyManagerApplication.class
    );
    DefaultProfileUtil.addDefaultProfile(app);
    Environment env = app.run(args).getEnvironment();
    logApplicationStartup(env);
  }

  private static void logApplicationStartup(Environment env) {
    String protocol = "http";
    String serverPort = env.getProperty("server.port");
    String contextPath = env.getProperty("server.servlet.context-path");
    if (StringUtils.isBlank(contextPath)) {
      contextPath = "/";
    }
    String hostAddress = "localhost";
    try {
      hostAddress = InetAddress.getLocalHost().getHostAddress();
    } catch (UnknownHostException e) {
      log.warn(
        "The host name could not be determined, using `localhost` as fallback"
      );
    }
    log.info(
      "\n----------------------------------------------------------\n\t" +
      "Application '{}' is running! Access URLs:\n\t" +
      "Local: \t\t{}://localhost:{}{}\n\t" +
      "External: \t{}://{}:{}{}\n\t" +
      "Profile(s): \t{}\n----------------------------------------------------------",
      env.getProperty("spring.application.name"),
      protocol,
      serverPort,
      contextPath,
      protocol,
      hostAddress,
      serverPort,
      contextPath,
      env.getActiveProfiles()
    );
  }
}
