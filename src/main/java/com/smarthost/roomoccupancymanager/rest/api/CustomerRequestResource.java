/*
 * Copyright (c) 2021 Fadi William Ghali Abdelmessih<fadi.william.ghali@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies
 * or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

package com.smarthost.roomoccupancymanager.rest.api;

import com.smarthost.roomoccupancymanager.rest.util.HeaderUtil;
import com.smarthost.roomoccupancymanager.rest.util.PaginationUtil;
import com.smarthost.roomoccupancymanager.rest.util.ResponseUtil;
import com.smarthost.roomoccupancymanager.service.CustomerRequestService;
import com.smarthost.roomoccupancymanager.service.dto.CustomerRequestDTO;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.smarthost.roomoccupancymanager.domain.CustomerRequest}.
 *
 * @author Fadi William Ghali Abdelmessih
 */
@RestController
@RequestMapping("/api")
@Tag(name = "CustomerRequestResource")
public class CustomerRequestResource {

  private final Logger log = LoggerFactory.getLogger(
    CustomerRequestResource.class
  );

  private static final String ENTITY_NAME = "customerRequest";

  @Value("${spring.application.name}")
  private String applicationName;

  private final CustomerRequestService customerRequestService;

  /**
   * Instantiates a new customer request resource.
   *
   * @param customerRequestService the customer request service
   */
  public CustomerRequestResource(
    CustomerRequestService customerRequestService
  ) {
    this.customerRequestService = customerRequestService;
  }

  /**
   * {@code POST  /customer-requests} : Create or update a customerRequest based on the properties.
   *
   * @param customerRequestDTO the customerRequestDTO to create.
   * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new customerRequestDTO, or with status {@code 400 (Bad Request)} if the customerRequest has already an ID.
   * @throws URISyntaxException if the Location URI syntax is incorrect.
   */
  @PostMapping("/customer-requests")
  public ResponseEntity<CustomerRequestDTO> createCustomerRequest(
    @Valid @RequestBody CustomerRequestDTO customerRequestDTO
  ) throws URISyntaxException {
    log.debug("REST request to save CustomerRequest : {}", customerRequestDTO);
    if (customerRequestDTO.getId() != null) {
      return ResponseEntity
        .badRequest()
        .header(
          "Failure",
          "A new customerRequest (Or the customerRequest to be updated) cannot already have an ID"
        )
        .body(null);
    }
    CustomerRequestDTO result = customerRequestService.save(customerRequestDTO);
    return ResponseEntity
      .ok()
      .headers(
        HeaderUtil.createOrUpdateEntityCreationAlert(
          applicationName,
          false,
          ENTITY_NAME,
          result.getId().toString()
        )
      )
      .body(result);
  }

  /**
   * {@code GET  /customer-requests/:customerId} : get all the customerRequests by customerId.
   *
   * @param pageable   the pagination information.
   * @param customerId the customer id
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of customerRequests in body.
   */
  @GetMapping("/customer-requests/{customerId}")
  public ResponseEntity<List<CustomerRequestDTO>> getAllCustomerRequestsByCustomerId(
    Pageable pageable,
    @PathVariable Long customerId
  ) {
    log.debug(
      "REST request to get a page of CustomerRequests based on the customerId: {}",
      customerId
    );
    Page<CustomerRequestDTO> page = customerRequestService.findAllByCustomerId(
      customerId,
      pageable
    );
    HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(
      ServletUriComponentsBuilder.fromCurrentRequest(),
      page
    );
    return ResponseEntity.ok().headers(headers).body(page.getContent());
  }

  /**
   * {@code GET  /customer-requests/:customerId/:date} : get all the customerRequests by date and customerId.
   *
   * @param pageable   the pagination information.
   * @param date       the date
   * @param customerId the customer id
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of customerRequests in body.
   */
  @GetMapping("/customer-requests/{customerId}/{date}")
  public ResponseEntity<List<CustomerRequestDTO>> getAllCustomerRequests(
    Pageable pageable,
    @PathVariable Long customerId,
    @PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date
  ) {
    log.debug(
      "REST request to get a page of CustomerRequests based on the date: {} & the customerId: {}",
      date,
      customerId
    );
    Page<CustomerRequestDTO> page = customerRequestService.findAllByDateAndCustomerId(
      date,
      customerId,
      pageable
    );
    HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(
      ServletUriComponentsBuilder.fromCurrentRequest(),
      page
    );
    return ResponseEntity.ok().headers(headers).body(page.getContent());
  }

  /**
   * {@code GET  /customer-requests/:date/:customerId/:hotelId} : get the customerRequest.
   *
   * @param date       the date
   * @param customerId the customer id
   * @param hotelId    the hotel id
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the customerRequestDTO, or with status {@code 404 (Not Found)}.
   */
  @GetMapping("/customer-requests/{date}/{customerId}/{hotelId}")
  public ResponseEntity<CustomerRequestDTO> getCustomerRequest(
    @PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date,
    @PathVariable Long customerId,
    @PathVariable Long hotelId
  ) {
    log.debug(
      "REST request to get CustomerRequest based on the date: {}, customerId: {} & hotelId: {}",
      date,
      customerId,
      hotelId
    );
    Optional<CustomerRequestDTO> customerRequestDTO = customerRequestService.findOneByDateAndCustomerIdAndHotelId(
      date,
      customerId,
      hotelId
    );
    return ResponseUtil.wrapOrNotFound(customerRequestDTO);
  }

  /**
   * {@code DELETE  /customer-requests/:date/:customerId/:hotelId} : delete the customerRequest.
   *
   * @param date       the date
   * @param customerId the customer id
   * @param hotelId    the hotel id
   * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
   */
  @DeleteMapping("/customer-requests/{date}/{customerId}/{hotelId}")
  public ResponseEntity<Void> deleteCustomerRequest(
    @PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date,
    @PathVariable Long customerId,
    @PathVariable Long hotelId
  ) {
    log.debug(
      "REST request to delete CustomerRequest based on the date: {}, customerId: {} & hotelId: {}",
      date,
      customerId,
      hotelId
    );
    Optional<CustomerRequestDTO> customerRequestDTO = customerRequestService.findOneByDateAndCustomerIdAndHotelId(
      date,
      customerId,
      hotelId
    );

    if (customerRequestDTO.isPresent()) {
      customerRequestService.deleteOneByDateAndCustomerIdAndHotelId(
        date,
        customerId,
        hotelId
      );
      return ResponseEntity
        .noContent()
        .headers(
          HeaderUtil.createEntityDeletionAlert(
            applicationName,
            false,
            ENTITY_NAME,
            customerRequestDTO.get().getId().toString()
          )
        )
        .build();
    } else {
      return ResponseEntity
        .badRequest()
        .header(
          "Failure",
          "There is no customerRequest that matches the arguments passed"
        )
        .body(null);
    }
  }
}
