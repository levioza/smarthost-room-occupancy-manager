/*
 * Copyright (c) 2021 Fadi William Ghali Abdelmessih<fadi.william.ghali@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies
 * or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

package com.smarthost.roomoccupancymanager.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A Hotel domain entity.
 *
 * @author Fadi William Ghali Abdelmessih
 */
@Entity
@Table(name = "hotel")
public class Hotel implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(
    strategy = GenerationType.SEQUENCE,
    generator = "sequenceGenerator"
  )
  @SequenceGenerator(name = "sequenceGenerator")
  private Long id;

  @NotNull
  @Column(name = "name", nullable = false)
  private String name;

  @OneToMany(mappedBy = "hotel")
  private Set<CustomerRequest> customerRequests = new HashSet<>();

  @OneToMany(mappedBy = "hotel")
  private Set<HotelOccupancy> hotelOccupancies = new HashSet<>();

  /**
   * Gets id.
   *
   * @return the id
   */
  public Long getId() {
    return id;
  }

  /**
   * Sets id.
   *
   * @param id the id
   */
  public void setId(Long id) {
    this.id = id;
  }

  /**
   * Gets name.
   *
   * @return the name
   */
  public String getName() {
    return name;
  }

  /**
   * Name hotel.
   *
   * @param name the name
   * @return the hotel
   */
  public Hotel name(String name) {
    this.name = name;
    return this;
  }

  /**
   * Sets name.
   *
   * @param name the name
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * Gets customer requests.
   *
   * @return the customer requests
   */
  public Set<CustomerRequest> getCustomerRequests() {
    return customerRequests;
  }

  /**
   * Customer requests hotel.
   *
   * @param customerRequests the customer requests
   * @return the hotel
   */
  public Hotel customerRequests(Set<CustomerRequest> customerRequests) {
    this.customerRequests = customerRequests;
    return this;
  }

  /**
   * Add customer request hotel.
   *
   * @param customerRequest the customer request
   * @return the hotel
   */
  public Hotel addCustomerRequest(CustomerRequest customerRequest) {
    this.customerRequests.add(customerRequest);
    customerRequest.setHotel(this);
    return this;
  }

  /**
   * Remove customer request hotel.
   *
   * @param customerRequest the customer request
   * @return the hotel
   */
  public Hotel removeCustomerRequest(CustomerRequest customerRequest) {
    this.customerRequests.remove(customerRequest);
    customerRequest.setHotel(null);
    return this;
  }

  /**
   * Sets customer requests.
   *
   * @param customerRequests the customer requests
   */
  public void setCustomerRequests(Set<CustomerRequest> customerRequests) {
    this.customerRequests = customerRequests;
  }

  /**
   * Gets hotel occupancies.
   *
   * @return the hotel occupancies
   */
  public Set<HotelOccupancy> getHotelOccupancies() {
    return hotelOccupancies;
  }

  /**
   * Hotel occupancies hotel.
   *
   * @param hotelOccupancies the hotel occupancies
   * @return the hotel
   */
  public Hotel hotelOccupancies(Set<HotelOccupancy> hotelOccupancies) {
    this.hotelOccupancies = hotelOccupancies;
    return this;
  }

  /**
   * Add hotel occupancy hotel.
   *
   * @param hotelOccupancy the hotel occupancy
   * @return the hotel
   */
  public Hotel addHotelOccupancy(HotelOccupancy hotelOccupancy) {
    this.hotelOccupancies.add(hotelOccupancy);
    hotelOccupancy.setHotel(this);
    return this;
  }

  /**
   * Remove hotel occupancy hotel.
   *
   * @param hotelOccupancy the hotel occupancy
   * @return the hotel
   */
  public Hotel removeHotelOccupancy(HotelOccupancy hotelOccupancy) {
    this.hotelOccupancies.remove(hotelOccupancy);
    hotelOccupancy.setHotel(null);
    return this;
  }

  /**
   * Sets hotel occupancies.
   *
   * @param hotelOccupancies the hotel occupancies
   */
  public void setHotelOccupancies(Set<HotelOccupancy> hotelOccupancies) {
    this.hotelOccupancies = hotelOccupancies;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Hotel)) {
      return false;
    }
    return id != null && id.equals(((Hotel) o).id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }

  @Override
  public String toString() {
    return "Hotel{" + "id=" + getId() + ", name='" + getName() + "'" + "}";
  }
}
