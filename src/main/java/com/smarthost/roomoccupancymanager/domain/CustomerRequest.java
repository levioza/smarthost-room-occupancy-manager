/*
 * Copyright (c) 2021 Fadi William Ghali Abdelmessih<fadi.william.ghali@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies
 * or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

package com.smarthost.roomoccupancymanager.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A CustomerRequest domain entity.
 *
 * @author Fadi William Ghali Abdelmessih
 */
@Entity
@Table(
  name = "customer_request",
  uniqueConstraints = @UniqueConstraint(
    columnNames = { "date", "customer_id", "hotel_id" }
  )
)
public class CustomerRequest implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(
    strategy = GenerationType.SEQUENCE,
    generator = "sequenceGenerator"
  )
  @SequenceGenerator(name = "sequenceGenerator")
  private Long id;

  @NotNull
  @Column(name = "date", nullable = false)
  private LocalDate date;

  @NotNull
  @Column(
    name = "amount_customer_willing_to_pay",
    precision = 100,
    scale = 2,
    nullable = false
  )
  private BigDecimal amountCustomerWillingToPay;

  @ManyToOne
  @JsonIgnoreProperties(value = "customerRequests", allowSetters = true)
  private Customer customer;

  @ManyToOne
  @JsonIgnoreProperties(value = "customerRequests", allowSetters = true)
  private Hotel hotel;

  /**
   * Gets id.
   *
   * @return the id
   */
  public Long getId() {
    return id;
  }

  /**
   * Sets id.
   *
   * @param id the id
   */
  public void setId(Long id) {
    this.id = id;
  }

  /**
   * Gets date.
   *
   * @return the date
   */
  public LocalDate getDate() {
    return date;
  }

  /**
   * Date customer request.
   *
   * @param date the date
   * @return the customer request
   */
  public CustomerRequest date(LocalDate date) {
    this.date = date;
    return this;
  }

  /**
   * Sets date.
   *
   * @param date the date
   */
  public void setDate(LocalDate date) {
    this.date = date;
  }

  /**
   * Gets amount customer willing to pay.
   *
   * @return the amount customer willing to pay
   */
  public BigDecimal getAmountCustomerWillingToPay() {
    return amountCustomerWillingToPay;
  }

  /**
   * Amount customer willing to pay customer request.
   *
   * @param amountCustomerWillingToPay the amount customer willing to pay
   * @return the customer request
   */
  public CustomerRequest amountCustomerWillingToPay(
    BigDecimal amountCustomerWillingToPay
  ) {
    this.amountCustomerWillingToPay = amountCustomerWillingToPay;
    return this;
  }

  /**
   * Sets amount customer willing to pay.
   *
   * @param amountCustomerWillingToPay the amount customer willing to pay
   */
  public void setAmountCustomerWillingToPay(
    BigDecimal amountCustomerWillingToPay
  ) {
    this.amountCustomerWillingToPay = amountCustomerWillingToPay;
  }

  /**
   * Gets customer.
   *
   * @return the customer
   */
  public Customer getCustomer() {
    return customer;
  }

  /**
   * Customer customer request.
   *
   * @param customer the customer
   * @return the customer request
   */
  public CustomerRequest customer(Customer customer) {
    this.customer = customer;
    return this;
  }

  /**
   * Sets customer.
   *
   * @param customer the customer
   */
  public void setCustomer(Customer customer) {
    this.customer = customer;
  }

  /**
   * Gets hotel.
   *
   * @return the hotel
   */
  public Hotel getHotel() {
    return hotel;
  }

  /**
   * Hotel customer request.
   *
   * @param hotel the hotel
   * @return the customer request
   */
  public CustomerRequest hotel(Hotel hotel) {
    this.hotel = hotel;
    return this;
  }

  /**
   * Sets hotel.
   *
   * @param hotel the hotel
   */
  public void setHotel(Hotel hotel) {
    this.hotel = hotel;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof CustomerRequest)) {
      return false;
    }
    return id != null && id.equals(((CustomerRequest) o).id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }

  @Override
  public String toString() {
    return (
      "CustomerRequest{" +
      "id=" +
      getId() +
      ", date='" +
      getDate() +
      "'" +
      ", amountCustomerWillingToPay=" +
      getAmountCustomerWillingToPay() +
      "}"
    );
  }
}
