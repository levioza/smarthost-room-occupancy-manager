/*
 * Copyright (c) 2021 Fadi William Ghali Abdelmessih<fadi.william.ghali@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies
 * or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

package com.smarthost.roomoccupancymanager.rest.api;

import com.smarthost.roomoccupancymanager.rest.util.HeaderUtil;
import com.smarthost.roomoccupancymanager.rest.util.PaginationUtil;
import com.smarthost.roomoccupancymanager.rest.util.ResponseUtil;
import com.smarthost.roomoccupancymanager.service.HotelService;
import com.smarthost.roomoccupancymanager.service.dto.HotelDTO;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.smarthost.roomoccupancymanager.domain.Hotel}.
 *
 * @author Fadi William Ghali Abdelmessih
 */
@RestController
@RequestMapping("/api")
@Tag(name = "HotelResource")
public class HotelResource {

  private final Logger log = LoggerFactory.getLogger(HotelResource.class);

  private static final String ENTITY_NAME = "hotel";

  @Value("${spring.application.name}")
  private String applicationName;

  private final HotelService hotelService;

  /**
   * Instantiates a new hotel resource.
   *
   * @param hotelService the hotel service
   */
  public HotelResource(HotelService hotelService) {
    this.hotelService = hotelService;
  }

  /**
   * {@code POST  /hotels} : Create a new hotel.
   *
   * @param hotelDTO the hotelDTO to create.
   * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new hotelDTO, or with status {@code 400 (Bad Request)} if the hotel has already an ID.
   * @throws URISyntaxException if the Location URI syntax is incorrect.
   */
  @PostMapping("/hotels")
  public ResponseEntity<HotelDTO> createHotel(
    @Valid @RequestBody HotelDTO hotelDTO
  ) throws URISyntaxException {
    log.debug("REST request to save Hotel : {}", hotelDTO);
    if (hotelDTO.getId() != null) {
      return ResponseEntity
        .badRequest()
        .header("Failure", "A new hotel cannot already have an ID")
        .body(null);
    }
    HotelDTO result = hotelService.save(hotelDTO);
    return ResponseEntity
      .created(new URI("/api/hotels/" + result.getId()))
      .headers(
        HeaderUtil.createEntityCreationAlert(
          applicationName,
          false,
          ENTITY_NAME,
          result.getId().toString()
        )
      )
      .body(result);
  }

  /**
   * {@code PUT  /hotels} : Updates an existing hotel.
   *
   * @param hotelDTO the hotelDTO to update.
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated hotelDTO, or with status {@code 400 (Bad Request)} if the hotelDTO is not valid, or with status {@code 500 (Internal Server Error)} if the hotelDTO couldn't be updated.
   * @throws URISyntaxException if the Location URI syntax is incorrect.
   */
  @PutMapping("/hotels")
  public ResponseEntity<HotelDTO> updateHotel(
    @Valid @RequestBody HotelDTO hotelDTO
  ) throws URISyntaxException {
    log.debug("REST request to update Hotel : {}", hotelDTO);
    if (hotelDTO.getId() == null) {
      return ResponseEntity
        .badRequest()
        .header("Failure", "The hotel id cannot be null")
        .body(null);
    }
    HotelDTO result = hotelService.save(hotelDTO);
    return ResponseEntity
      .ok()
      .headers(
        HeaderUtil.createEntityUpdateAlert(
          applicationName,
          false,
          ENTITY_NAME,
          hotelDTO.getId().toString()
        )
      )
      .body(result);
  }

  /**
   * {@code GET  /hotels} : get all the hotels.
   *
   * @param pageable the pagination information.
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of hotels in body.
   */
  @GetMapping("/hotels")
  public ResponseEntity<List<HotelDTO>> getAllHotels(Pageable pageable) {
    log.debug("REST request to get a page of Hotels");
    Page<HotelDTO> page = hotelService.findAll(pageable);
    HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(
      ServletUriComponentsBuilder.fromCurrentRequest(),
      page
    );
    return ResponseEntity.ok().headers(headers).body(page.getContent());
  }

  /**
   * {@code GET  /hotels/:id} : get the "id" hotel.
   *
   * @param id the id of the hotelDTO to retrieve.
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the hotelDTO, or with status {@code 404 (Not Found)}.
   */
  @GetMapping("/hotels/{id}")
  public ResponseEntity<HotelDTO> getHotel(@PathVariable Long id) {
    log.debug("REST request to get Hotel : {}", id);
    Optional<HotelDTO> hotelDTO = hotelService.findOne(id);
    return ResponseUtil.wrapOrNotFound(hotelDTO);
  }

  /**
   * {@code DELETE  /hotels/:id} : delete the "id" hotel.
   *
   * @param id the id of the hotelDTO to delete.
   * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
   */
  @DeleteMapping("/hotels/{id}")
  public ResponseEntity<Void> deleteHotel(@PathVariable Long id) {
    log.debug("REST request to delete Hotel : {}", id);
    hotelService.delete(id);
    return ResponseEntity
      .noContent()
      .headers(
        HeaderUtil.createEntityDeletionAlert(
          applicationName,
          false,
          ENTITY_NAME,
          id.toString()
        )
      )
      .build();
  }
}
