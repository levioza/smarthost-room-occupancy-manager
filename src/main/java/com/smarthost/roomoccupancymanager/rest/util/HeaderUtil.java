/*
 * Copyright 2016 the original j-hipster author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.smarthost.roomoccupancymanager.rest.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

/**
 * A header utility class used in the REST controllers.
 */
public final class HeaderUtil {

  private static final Logger log = LoggerFactory.getLogger(HeaderUtil.class);

  private HeaderUtil() {}

  /**
   * Create an alert's http headers.
   *
   * @param applicationName the application name
   * @param message         the message
   * @param param           the param
   * @return the http headers
   */
  public static HttpHeaders createAlert(
    String applicationName,
    String message,
    String param
  ) {
    HttpHeaders headers = new HttpHeaders();
    headers.add("X-" + applicationName + "-alert", message);

    try {
      headers.add(
        "X-" + applicationName + "-params",
        URLEncoder.encode(param, StandardCharsets.UTF_8.toString())
      );
    } catch (UnsupportedEncodingException unsupportedEncodingException) {
      // Do nothing.
    }

    return headers;
  }

  /**
   * Create an entity creation alert's http headers.
   *
   * @param applicationName   the application name
   * @param enableTranslation the enable translation
   * @param entityName        the entity name
   * @param param             the param
   * @return the http headers
   */
  public static HttpHeaders createEntityCreationAlert(
    String applicationName,
    boolean enableTranslation,
    String entityName,
    String param
  ) {
    String message = enableTranslation
      ? applicationName + "." + entityName + ".created"
      : "A new " + entityName + " is created with identifier " + param;
    return createAlert(applicationName, message, param);
  }

  /**
   * Create or update entity alert's http headers.
   *
   * @param applicationName   the application name
   * @param enableTranslation the enable translation
   * @param entityName        the entity name
   * @param param             the param
   * @return the http headers
   */
  public static HttpHeaders createOrUpdateEntityCreationAlert(
    String applicationName,
    boolean enableTranslation,
    String entityName,
    String param
  ) {
    String message = enableTranslation
      ? applicationName + "." + entityName + ".createdOrUpdated"
      : "The " +
      entityName +
      " with identifier " +
      param +
      " has been created or updated";
    return createAlert(applicationName, message, param);
  }

  /**
   * Create an entity update alert's http headers.
   *
   * @param applicationName   the application name
   * @param enableTranslation the enable translation
   * @param entityName        the entity name
   * @param param             the param
   * @return the http headers
   */
  public static HttpHeaders createEntityUpdateAlert(
    String applicationName,
    boolean enableTranslation,
    String entityName,
    String param
  ) {
    String message = enableTranslation
      ? applicationName + "." + entityName + ".updated"
      : "A " + entityName + " is updated with identifier " + param;
    return createAlert(applicationName, message, param);
  }

  /**
   * Create an entity deletion alert's http headers.
   *
   * @param applicationName   the application name
   * @param enableTranslation the enable translation
   * @param entityName        the entity name
   * @param param             the param
   * @return the http headers
   */
  public static HttpHeaders createEntityDeletionAlert(
    String applicationName,
    boolean enableTranslation,
    String entityName,
    String param
  ) {
    String message = enableTranslation
      ? applicationName + "." + entityName + ".deleted"
      : "A " + entityName + " is deleted with identifier " + param;
    return createAlert(applicationName, message, param);
  }

  /**
   * Create a failure alert's http headers.
   *
   * @param applicationName   the application name
   * @param enableTranslation the enable translation
   * @param entityName        the entity name
   * @param errorKey          the error key
   * @param defaultMessage    the default message
   * @return the http headers
   */
  public static HttpHeaders createFailureAlert(
    String applicationName,
    boolean enableTranslation,
    String entityName,
    String errorKey,
    String defaultMessage
  ) {
    log.error("Entity processing failed, {}", defaultMessage);
    String message = enableTranslation ? "error." + errorKey : defaultMessage;
    HttpHeaders headers = new HttpHeaders();
    headers.add("X-" + applicationName + "-error", message);
    headers.add("X-" + applicationName + "-params", entityName);
    return headers;
  }
}
