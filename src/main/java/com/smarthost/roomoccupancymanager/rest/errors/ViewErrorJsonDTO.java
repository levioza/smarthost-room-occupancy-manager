/*
 * Copyright (c) 2021 Fadi William Ghali Abdelmessih<fadi.william.ghali@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies
 * or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

package com.smarthost.roomoccupancymanager.rest.errors;

/**
 * The View error json dto.
 *
 * @author Fadi William Ghali Abdelmessih
 */
public class ViewErrorJsonDTO {

  private Integer status;
  private String error;
  private String message;
  private String timeStamp;
  private String trace;
  private String path;
  private String reportTo;
  private String friendlyMessage;

  /**
   * Instantiates a new view error json dto.
   *
   * @param status    the status
   * @param error     the error
   * @param message   the message
   * @param timeStamp the time stamp
   * @param trace     the trace
   * @param path      the path
   */
  public ViewErrorJsonDTO(
    Integer status,
    String error,
    String message,
    String timeStamp,
    String trace,
    String path
  ) {
    this.status = status;
    this.error = error;
    this.message = message;
    this.timeStamp = timeStamp;
    this.trace = trace;
    this.path = path;
    this.reportTo = "fadi.william.ghali@gmail.com";
    this.friendlyMessage =
      "If you believe that this due to an error. Please capture a snapshot of this page and send it to the provided email.";
  }

  /**
   * Gets status.
   *
   * @return the status
   */
  public Integer getStatus() {
    return status;
  }

  /**
   * Sets status.
   *
   * @param status the status
   */
  public void setStatus(Integer status) {
    this.status = status;
  }

  /**
   * Gets error.
   *
   * @return the error
   */
  public String getError() {
    return error;
  }

  /**
   * Sets error.
   *
   * @param error the error
   */
  public void setError(String error) {
    this.error = error;
  }

  /**
   * Gets message.
   *
   * @return the message
   */
  public String getMessage() {
    return message;
  }

  /**
   * Sets message.
   *
   * @param message the message
   */
  public void setMessage(String message) {
    this.message = message;
  }

  /**
   * Gets time stamp.
   *
   * @return the time stamp
   */
  public String getTimeStamp() {
    return timeStamp;
  }

  /**
   * Sets time stamp.
   *
   * @param timeStamp the time stamp
   */
  public void setTimeStamp(String timeStamp) {
    this.timeStamp = timeStamp;
  }

  /**
   * Gets trace.
   *
   * @return the trace
   */
  public String getTrace() {
    return trace;
  }

  /**
   * Sets trace.
   *
   * @param trace the trace
   */
  public void setTrace(String trace) {
    this.trace = trace;
  }

  /**
   * Gets path.
   *
   * @return the path
   */
  public String getPath() {
    return path;
  }

  /**
   * Sets path.
   *
   * @param path the path
   */
  public void setPath(String path) {
    this.path = path;
  }

  /**
   * Gets report to.
   *
   * @return the report to
   */
  public String getReportTo() {
    return reportTo;
  }

  /**
   * Sets report to.
   *
   * @param reportTo the report to
   */
  public void setReportTo(String reportTo) {
    this.reportTo = reportTo;
  }

  /**
   * Gets friendly message.
   *
   * @return the friendly message
   */
  public String getFriendlyMessage() {
    return friendlyMessage;
  }

  /**
   * Sets friendly message.
   *
   * @param friendlyMessage the friendly message
   */
  public void setFriendlyMessage(String friendlyMessage) {
    this.friendlyMessage = friendlyMessage;
  }

  @Override
  public String toString() {
    return (
      "ViewErrorJsonDTO{" +
      "status=" +
      status +
      ", error='" +
      error +
      '\'' +
      ", message='" +
      message +
      '\'' +
      ", timeStamp='" +
      timeStamp +
      '\'' +
      ", trace='" +
      trace +
      '\'' +
      ", path='" +
      path +
      '\'' +
      ", reportTo='" +
      reportTo +
      '\'' +
      ", friendlyMessage='" +
      friendlyMessage +
      '\'' +
      '}'
    );
  }
}
