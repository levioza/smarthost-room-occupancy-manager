/*
 * Copyright (c) 2021 Fadi William Ghali Abdelmessih<fadi.william.ghali@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies
 * or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

package com.smarthost.roomoccupancymanager.service.impl;

import com.smarthost.roomoccupancymanager.domain.Hotel;
import com.smarthost.roomoccupancymanager.repository.HotelRepository;
import com.smarthost.roomoccupancymanager.service.HotelService;
import com.smarthost.roomoccupancymanager.service.dto.HotelDTO;
import com.smarthost.roomoccupancymanager.service.mapper.HotelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Hotel}.
 *
 * @author Fadi william Ghali Abdelmessih
 */
@Service
@Transactional
public class HotelServiceImpl implements HotelService {

  private final Logger log = LoggerFactory.getLogger(HotelServiceImpl.class);

  private final HotelRepository hotelRepository;

  private final HotelMapper hotelMapper;

  /**
   * Instantiates a new hotel service.
   *
   * @param hotelRepository the hotel repository
   * @param hotelMapper     the hotel mapper
   */
  public HotelServiceImpl(
    HotelRepository hotelRepository,
    HotelMapper hotelMapper
  ) {
    this.hotelRepository = hotelRepository;
    this.hotelMapper = hotelMapper;
  }

  @Override
  public HotelDTO save(HotelDTO hotelDTO) {
    log.debug("Request to save Hotel : {}", hotelDTO);
    Hotel hotel = hotelMapper.toEntity(hotelDTO);
    hotel = hotelRepository.save(hotel);
    return hotelMapper.toDto(hotel);
  }

  @Override
  @Transactional(readOnly = true)
  public Page<HotelDTO> findAll(Pageable pageable) {
    log.debug("Request to get all Hotels");
    return hotelRepository.findAll(pageable).map(hotelMapper::toDto);
  }

  @Override
  @Transactional(readOnly = true)
  public Optional<HotelDTO> findOne(Long id) {
    log.debug("Request to get Hotel : {}", id);
    return hotelRepository.findById(id).map(hotelMapper::toDto);
  }

  @Override
  public void delete(Long id) {
    log.debug("Request to delete Hotel : {}", id);
    hotelRepository.deleteById(id);
  }
}
