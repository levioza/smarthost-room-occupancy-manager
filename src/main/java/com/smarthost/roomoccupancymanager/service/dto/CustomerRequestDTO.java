/*
 * Copyright (c) 2021 Fadi William Ghali Abdelmessih<fadi.william.ghali@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies
 * or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

package com.smarthost.roomoccupancymanager.service.dto;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A DTO for the {@link com.smarthost.roomoccupancymanager.domain.CustomerRequest} entity.
 *
 * @author Fadi William Ghali Abdelmessih
 */
public class CustomerRequestDTO implements Serializable {

  private Long id;

  @NotNull
  private LocalDate date;

  @NotNull
  private BigDecimal amountCustomerWillingToPay;

  @NotNull
  private Long customerId;

  @NotNull
  private Long hotelId;

  /**
   * Gets id.
   *
   * @return the id
   */
  public Long getId() {
    return id;
  }

  /**
   * Sets id.
   *
   * @param id the id
   */
  public void setId(Long id) {
    this.id = id;
  }

  /**
   * Gets date.
   *
   * @return the date
   */
  public LocalDate getDate() {
    return date;
  }

  /**
   * Sets date.
   *
   * @param date the date
   */
  public void setDate(LocalDate date) {
    this.date = date;
  }

  /**
   * Gets amount customer willing to pay.
   *
   * @return the amount customer willing to pay
   */
  public BigDecimal getAmountCustomerWillingToPay() {
    return amountCustomerWillingToPay;
  }

  /**
   * Sets amount customer willing to pay.
   *
   * @param amountCustomerWillingToPay the amount customer willing to pay
   */
  public void setAmountCustomerWillingToPay(
    BigDecimal amountCustomerWillingToPay
  ) {
    this.amountCustomerWillingToPay = amountCustomerWillingToPay;
  }

  /**
   * Gets customer id.
   *
   * @return the customer id
   */
  public Long getCustomerId() {
    return customerId;
  }

  /**
   * Sets customer id.
   *
   * @param customerId the customer id
   */
  public void setCustomerId(Long customerId) {
    this.customerId = customerId;
  }

  /**
   * Gets hotel id.
   *
   * @return the hotel id
   */
  public Long getHotelId() {
    return hotelId;
  }

  /**
   * Sets hotel id.
   *
   * @param hotelId the hotel id
   */
  public void setHotelId(Long hotelId) {
    this.hotelId = hotelId;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    CustomerRequestDTO that = (CustomerRequestDTO) o;
    return (
      date.equals(that.date) &&
      customerId.equals(that.customerId) &&
      hotelId.equals(that.hotelId)
    );
  }

  @Override
  public int hashCode() {
    return Objects.hash(date, customerId, hotelId);
  }

  @Override
  public String toString() {
    return (
      "CustomerRequestDTO{" +
      "id=" +
      getId() +
      ", date='" +
      getDate() +
      "'" +
      ", amountCustomerWillingToPay=" +
      getAmountCustomerWillingToPay() +
      ", customerId=" +
      getCustomerId() +
      ", hotelId=" +
      getHotelId() +
      "}"
    );
  }
}
