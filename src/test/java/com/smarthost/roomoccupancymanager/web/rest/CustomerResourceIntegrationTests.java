/*
 * Copyright (c) 2021 Fadi William Ghali Abdelmessih<fadi.william.ghali@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies
 * or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

package com.smarthost.roomoccupancymanager.web.rest;

import com.smarthost.roomoccupancymanager.RoomOccupancyManagerApplication;
import com.smarthost.roomoccupancymanager.domain.Customer;
import com.smarthost.roomoccupancymanager.repository.CustomerRepository;
import com.smarthost.roomoccupancymanager.service.dto.CustomerDTO;
import com.smarthost.roomoccupancymanager.service.mapper.CustomerMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link CustomerResource} REST controller.
 *
 * @author Fadi William Ghali Abdelmessih
 */
@SpringBootTest(classes = RoomOccupancyManagerApplication.class)
@AutoConfigureMockMvc
class CustomerResourceIntegrationTests {

  private static final String DEFAULT_FULL_NAME = "Wilhelmina Warner";
  private static final String UPDATED_FULL_NAME = "Wilhelmina Warner Bros";

  @Autowired
  private CustomerRepository customerRepository;

  @Autowired
  private CustomerMapper customerMapper;

  @Autowired
  private EntityManager em;

  @Autowired
  private MockMvc restCustomerMockMvc;

  private Customer customer;

  /**
   * Create an entity for this test.
   *
   * @return the customer
   */
  public static Customer createEntity() {
    Customer customer = new Customer().fullName(DEFAULT_FULL_NAME);
    return customer;
  }

  /**
   * Create an updated entity for this test.
   *
   * @return the customer
   */
  public static Customer createUpdatedEntity() {
    Customer customer = new Customer().fullName(UPDATED_FULL_NAME);
    return customer;
  }

  /**
   * Init test.
   */
  @BeforeEach
  public void initTest() {
    customer = createEntity();
  }

  /**
   * Create customer.
   *
   * @throws Exception the exception
   */
  @Test
  @Transactional
  void createCustomer() throws Exception {
    int databaseSizeBeforeCreate = customerRepository.findAll().size();
    // Create the Customer.
    CustomerDTO customerDTO = customerMapper.toDto(customer);
    restCustomerMockMvc
      .perform(
        post("/api/customers")
          .contentType(MediaType.APPLICATION_JSON)
          .content(TestUtil.convertObjectToJsonBytes(customerDTO))
      )
      .andExpect(status().isCreated());

    // Validate the Customer in the database.
    List<Customer> customerList = customerRepository.findAll();
    assertThat(customerList).hasSize(databaseSizeBeforeCreate + 1);
    Customer testCustomer = customerList.get(customerList.size() - 1);
    assertThat(testCustomer.getFullName()).isEqualTo(DEFAULT_FULL_NAME);
  }

  /**
   * Create customer with existing id.
   *
   * @throws Exception the exception
   */
  @Test
  @Transactional
  void createCustomerWithExistingId() throws Exception {
    int databaseSizeBeforeCreate = customerRepository.findAll().size();

    // Create the Customer with an existing ID.
    customer.setId(1L);
    CustomerDTO customerDTO = customerMapper.toDto(customer);

    // An entity with an existing ID cannot be created, so this API call must fail.
    restCustomerMockMvc
      .perform(
        post("/api/customers")
          .contentType(MediaType.APPLICATION_JSON)
          .content(TestUtil.convertObjectToJsonBytes(customerDTO))
      )
      .andExpect(status().isBadRequest());

    // Validate the Customer in the database.
    List<Customer> customerList = customerRepository.findAll();
    assertThat(customerList).hasSize(databaseSizeBeforeCreate);
  }

  /**
   * Check full name is required.
   *
   * @throws Exception the exception
   */
  @Test
  @Transactional
  void checkFullNameIsRequired() throws Exception {
    int databaseSizeBeforeTest = customerRepository.findAll().size();
    // Set the field to null.
    customer.setFullName(null);

    // Create the Customer, which fails.
    CustomerDTO customerDTO = customerMapper.toDto(customer);

    restCustomerMockMvc
      .perform(
        post("/api/customers")
          .contentType(MediaType.APPLICATION_JSON)
          .content(TestUtil.convertObjectToJsonBytes(customerDTO))
      )
      .andExpect(status().isBadRequest());

    List<Customer> customerList = customerRepository.findAll();
    assertThat(customerList).hasSize(databaseSizeBeforeTest);
  }

  /**
   * Gets all customers.
   *
   * @throws Exception the exception
   */
  @Test
  @Transactional
  void getAllCustomers() throws Exception {
    // Initialize the database.
    customerRepository.saveAndFlush(customer);

    // Get all the customerList.
    restCustomerMockMvc
      .perform(get("/api/customers?sort=id,desc"))
      .andExpect(status().isOk())
      .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
      .andExpect(
        jsonPath("$.[*].id").value(hasItem(customer.getId().intValue()))
      )
      .andExpect(jsonPath("$.[*].fullName").value(hasItem(DEFAULT_FULL_NAME)));
  }

  /**
   * Gets customer.
   *
   * @throws Exception the exception
   */
  @Test
  @Transactional
  void getCustomer() throws Exception {
    // Initialize the database.
    customerRepository.saveAndFlush(customer);

    // Get the customer.
    restCustomerMockMvc
      .perform(get("/api/customers/{id}", customer.getId()))
      .andExpect(status().isOk())
      .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
      .andExpect(jsonPath("$.id").value(customer.getId().intValue()))
      .andExpect(jsonPath("$.fullName").value(DEFAULT_FULL_NAME));
  }

  /**
   * Gets non existing customer.
   *
   * @throws Exception the exception
   */
  @Test
  @Transactional
  void getNonExistingCustomer() throws Exception {
    // Get the customer.
    restCustomerMockMvc
      .perform(get("/api/customers/{id}", Long.MAX_VALUE))
      .andExpect(status().isNotFound());
  }

  /**
   * Update customer.
   *
   * @throws Exception the exception
   */
  @Test
  @Transactional
  void updateCustomer() throws Exception {
    // Initialize the database.
    customerRepository.saveAndFlush(customer);

    int databaseSizeBeforeUpdate = customerRepository.findAll().size();

    // Update the customer.
    Customer updatedCustomer = customerRepository
      .findById(customer.getId())
      .get();

    // Disconnect from session so that the updates on updatedCustomer are not directly saved in db.
    em.detach(updatedCustomer);

    updatedCustomer.fullName(UPDATED_FULL_NAME);
    CustomerDTO customerDTO = customerMapper.toDto(updatedCustomer);

    restCustomerMockMvc
      .perform(
        put("/api/customers")
          .contentType(MediaType.APPLICATION_JSON)
          .content(TestUtil.convertObjectToJsonBytes(customerDTO))
      )
      .andExpect(status().isOk());

    // Validate the Customer in the database.
    List<Customer> customerList = customerRepository.findAll();
    assertThat(customerList).hasSize(databaseSizeBeforeUpdate);
    Customer testCustomer = customerList.get(customerList.size() - 1);
    assertThat(testCustomer.getFullName()).isEqualTo(UPDATED_FULL_NAME);
  }

  /**
   * Update non existing customer.
   *
   * @throws Exception the exception
   */
  @Test
  @Transactional
  void updateNonExistingCustomer() throws Exception {
    int databaseSizeBeforeUpdate = customerRepository.findAll().size();

    // Create the Customer.
    CustomerDTO customerDTO = customerMapper.toDto(customer);

    // If the entity doesn't have an ID, it will throw a BadRequest.
    restCustomerMockMvc
      .perform(
        put("/api/customers")
          .contentType(MediaType.APPLICATION_JSON)
          .content(TestUtil.convertObjectToJsonBytes(customerDTO))
      )
      .andExpect(status().isBadRequest());

    // Validate the Customer in the database.
    List<Customer> customerList = customerRepository.findAll();
    assertThat(customerList).hasSize(databaseSizeBeforeUpdate);
  }

  /**
   * Delete customer.
   *
   * @throws Exception the exception
   */
  @Test
  @Transactional
  void deleteCustomer() throws Exception {
    // Initialize the database.
    customerRepository.saveAndFlush(customer);

    int databaseSizeBeforeDelete = customerRepository.findAll().size();

    // Delete the customer.
    restCustomerMockMvc
      .perform(
        delete("/api/customers/{id}", customer.getId())
          .accept(MediaType.APPLICATION_JSON)
      )
      .andExpect(status().isNoContent());

    // Validate that the database contains one less item.
    List<Customer> customerList = customerRepository.findAll();
    assertThat(customerList).hasSize(databaseSizeBeforeDelete - 1);
  }
}
