/*
 * Copyright (c) 2021 Fadi William Ghali Abdelmessih<fadi.william.ghali@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies
 * or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

package com.smarthost.roomoccupancymanager.service.impl;

import com.smarthost.roomoccupancymanager.domain.HotelOccupancy;
import com.smarthost.roomoccupancymanager.repository.HotelOccupancyRepository;
import com.smarthost.roomoccupancymanager.service.HotelOccupancyService;
import com.smarthost.roomoccupancymanager.service.dto.HotelOccupancyDTO;
import com.smarthost.roomoccupancymanager.service.mapper.HotelOccupancyMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.Optional;

/**
 * Service Implementation for managing {@link HotelOccupancy}.
 *
 * @author Fadi william Ghali Abdelmessih
 */
@Service
@Transactional
public class HotelOccupancyServiceImpl implements HotelOccupancyService {

  private final Logger log = LoggerFactory.getLogger(
    HotelOccupancyServiceImpl.class
  );

  private final HotelOccupancyRepository hotelOccupancyRepository;

  private final HotelOccupancyMapper hotelOccupancyMapper;

  /**
   * Instantiates a new hotel occupancy service.
   *
   * @param hotelOccupancyRepository the hotel occupancy repository
   * @param hotelOccupancyMapper     the hotel occupancy mapper
   */
  public HotelOccupancyServiceImpl(
    HotelOccupancyRepository hotelOccupancyRepository,
    HotelOccupancyMapper hotelOccupancyMapper
  ) {
    this.hotelOccupancyRepository = hotelOccupancyRepository;
    this.hotelOccupancyMapper = hotelOccupancyMapper;
  }

  @Override
  public HotelOccupancyDTO save(HotelOccupancyDTO hotelOccupancyDTO) {
    log.debug("Request to save HotelOccupancy : {}", hotelOccupancyDTO);
    HotelOccupancy hotelOccupancy = hotelOccupancyMapper.toEntity(
      hotelOccupancyDTO
    );

    // Check if there's a customer request already for the same customer at the same hotel on the same date.
    Optional<HotelOccupancy> hotelOccupancyThatAlreadyExists = hotelOccupancyRepository.findOneByDateAndHotel_id(
      hotelOccupancy.getDate(),
      hotelOccupancy.getHotel().getId()
    );

    if (hotelOccupancyThatAlreadyExists.isPresent()) {
      // Set the id of the customer request object to perform an upsert.
      hotelOccupancy.setId(hotelOccupancyThatAlreadyExists.get().getId());
    }

    hotelOccupancy = hotelOccupancyRepository.save(hotelOccupancy);
    return hotelOccupancyMapper.toDto(hotelOccupancy);
  }

  @Override
  public Page<HotelOccupancyDTO> findAllByHotelId(
    Long hotelId,
    Pageable pageable
  ) {
    log.debug(
      "Request to get all HotelOccupancies based on the hotelId: {}",
      hotelId
    );
    return hotelOccupancyRepository
      .findAllByHotel_id(hotelId, pageable)
      .map(hotelOccupancyMapper::toDto);
  }

  @Override
  public Optional<HotelOccupancyDTO> findOneByDateAndHotelId(
    LocalDate date,
    Long hotelId
  ) {
    log.debug(
      "Request to get HotelOccupancy based on the date: {} & hotelId: {}",
      date,
      hotelId
    );
    return hotelOccupancyRepository
      .findOneByDateAndHotel_id(date, hotelId)
      .map(hotelOccupancyMapper::toDto);
  }

  @Override
  public void deleteOneByDateAndHotelId(LocalDate date, Long hotelId) {
    log.debug(
      "Request to delete HotelOccupancy based on the date: {} & hotelId: {}",
      date,
      hotelId
    );
    hotelOccupancyRepository.deleteOneByDateAndHotel_id(date, hotelId);
  }
}
