/*
 * Copyright (c) 2021 Fadi William Ghali Abdelmessih<fadi.william.ghali@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies
 * or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

package com.smarthost.roomoccupancymanager.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A Customer domain entity.
 *
 * @author Fadi William Ghali Abdelmessih
 */
@Entity
@Table(name = "customer")
public class Customer implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(
    strategy = GenerationType.SEQUENCE,
    generator = "sequenceGenerator"
  )
  @SequenceGenerator(name = "sequenceGenerator")
  private Long id;

  @NotNull
  @Column(name = "full_name", nullable = false)
  private String fullName;

  @OneToMany(mappedBy = "customer")
  private Set<CustomerRequest> customerRequests = new HashSet<>();

  /**
   * Gets id.
   *
   * @return the id
   */
  public Long getId() {
    return id;
  }

  /**
   * Sets id.
   *
   * @param id the id
   */
  public void setId(Long id) {
    this.id = id;
  }

  /**
   * Gets full name.
   *
   * @return the full name
   */
  public String getFullName() {
    return fullName;
  }

  /**
   * Full name customer.
   *
   * @param fullName the full name
   * @return the customer
   */
  public Customer fullName(String fullName) {
    this.fullName = fullName;
    return this;
  }

  /**
   * Sets full name.
   *
   * @param fullName the full name
   */
  public void setFullName(String fullName) {
    this.fullName = fullName;
  }

  /**
   * Gets customer requests.
   *
   * @return the customer requests
   */
  public Set<CustomerRequest> getCustomerRequests() {
    return customerRequests;
  }

  /**
   * Customer requests customer.
   *
   * @param customerRequests the customer requests
   * @return the customer
   */
  public Customer customerRequests(Set<CustomerRequest> customerRequests) {
    this.customerRequests = customerRequests;
    return this;
  }

  /**
   * Add customer request customer.
   *
   * @param customerRequest the customer request
   * @return the customer
   */
  public Customer addCustomerRequest(CustomerRequest customerRequest) {
    this.customerRequests.add(customerRequest);
    customerRequest.setCustomer(this);
    return this;
  }

  /**
   * Remove customer request customer.
   *
   * @param customerRequest the customer request
   * @return the customer
   */
  public Customer removeCustomerRequest(CustomerRequest customerRequest) {
    this.customerRequests.remove(customerRequest);
    customerRequest.setCustomer(null);
    return this;
  }

  /**
   * Sets customer requests.
   *
   * @param customerRequests the customer requests
   */
  public void setCustomerRequests(Set<CustomerRequest> customerRequests) {
    this.customerRequests = customerRequests;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Customer)) {
      return false;
    }
    return id != null && id.equals(((Customer) o).id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }

  @Override
  public String toString() {
    return (
      "Customer{" + "id=" + getId() + ", fullName='" + getFullName() + "'" + "}"
    );
  }
}
