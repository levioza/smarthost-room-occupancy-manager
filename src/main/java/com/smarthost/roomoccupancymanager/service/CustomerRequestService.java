/*
 * Copyright (c) 2021 Fadi William Ghali Abdelmessih<fadi.william.ghali@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies
 * or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

package com.smarthost.roomoccupancymanager.service;

import com.smarthost.roomoccupancymanager.service.dto.CustomerRequestDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.time.LocalDate;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.smarthost.roomoccupancymanager.domain.CustomerRequest}.
 *
 * @author Fadi william Ghali Abdelmessih
 */
public interface CustomerRequestService {
  /**
   * Save a customerRequest.
   *
   * @param customerRequestDTO the entity to save.
   * @return the persisted entity.
   */
  CustomerRequestDTO save(CustomerRequestDTO customerRequestDTO);

  /**
   * Find all by customer id.
   *
   * @param customerId the customer id
   * @return the list of customerRequestDTO
   */
  Page<CustomerRequestDTO> findAllByCustomerId(
    Long customerId,
    Pageable pageable
  );

  /**
   * Find all by date and customer id.
   *
   * @param date       the date
   * @param customerId the customer id
   * @return the list of customerRequestDTO
   */
  Page<CustomerRequestDTO> findAllByDateAndCustomerId(
    LocalDate date,
    Long customerId,
    Pageable pageable
  );

  /**
   * Find one by date and customer id and hotel id.
   *
   * @param date       the date
   * @param customerId the customer id
   * @param hotelId    the hotel id
   * @return the optional customerRequestDTO
   */
  Optional<CustomerRequestDTO> findOneByDateAndCustomerIdAndHotelId(
    LocalDate date,
    Long customerId,
    Long hotelId
  );

  /**
   * Delete one by date and customer id and hotel id.
   *
   * @param date       the date
   * @param customerId the customer id
   * @param hotelId    the hotel id
   */
  void deleteOneByDateAndCustomerIdAndHotelId(
    LocalDate date,
    Long customerId,
    Long hotelId
  );
}
