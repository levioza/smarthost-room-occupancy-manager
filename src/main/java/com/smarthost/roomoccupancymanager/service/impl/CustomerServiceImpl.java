/*
 * Copyright (c) 2021 Fadi William Ghali Abdelmessih<fadi.william.ghali@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies
 * or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

package com.smarthost.roomoccupancymanager.service.impl;

import com.smarthost.roomoccupancymanager.domain.Customer;
import com.smarthost.roomoccupancymanager.repository.CustomerRepository;
import com.smarthost.roomoccupancymanager.service.CustomerService;
import com.smarthost.roomoccupancymanager.service.dto.CustomerDTO;
import com.smarthost.roomoccupancymanager.service.mapper.CustomerMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Customer}.
 *
 * @author Fadi william Ghali Abdelmessih
 */
@Service
@Transactional
public class CustomerServiceImpl implements CustomerService {

  private final Logger log = LoggerFactory.getLogger(CustomerServiceImpl.class);

  private final CustomerRepository customerRepository;

  private final CustomerMapper customerMapper;

  /**
   * Instantiates a new customer service.
   *
   * @param customerRepository the customer repository
   * @param customerMapper     the customer mapper
   */
  public CustomerServiceImpl(
    CustomerRepository customerRepository,
    CustomerMapper customerMapper
  ) {
    this.customerRepository = customerRepository;
    this.customerMapper = customerMapper;
  }

  @Override
  public CustomerDTO save(CustomerDTO customerDTO) {
    log.debug("Request to save Customer : {}", customerDTO);
    Customer customer = customerMapper.toEntity(customerDTO);

    customer = customerRepository.save(customer);
    return customerMapper.toDto(customer);
  }

  @Override
  @Transactional(readOnly = true)
  public Page<CustomerDTO> findAll(Pageable pageable) {
    log.debug("Request to get all Customers");
    return customerRepository.findAll(pageable).map(customerMapper::toDto);
  }

  @Override
  @Transactional(readOnly = true)
  public Optional<CustomerDTO> findOne(Long id) {
    log.debug("Request to get Customer : {}", id);
    return customerRepository.findById(id).map(customerMapper::toDto);
  }

  @Override
  public void delete(Long id) {
    log.debug("Request to delete Customer : {}", id);
    customerRepository.deleteById(id);
  }
}
