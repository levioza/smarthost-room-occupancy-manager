/*
 * Copyright 2016 the original j-hipster author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.smarthost.roomoccupancymanager.rest.util;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

/**
 * A response utility class used in the REST controllers.
 */
public interface ResponseUtil {
  /**
   * Wrap or not found response entity.
   *
   * @param <X>           the type parameter
   * @param maybeResponse the maybe response
   * @return the response entity
   */
  static <X> ResponseEntity<X> wrapOrNotFound(Optional<X> maybeResponse) {
    return wrapOrNotFound(maybeResponse, (HttpHeaders) null);
  }

  /**
   * Wrap or not found response entity.
   *
   * @param <X>           the type parameter
   * @param maybeResponse the maybe response
   * @param header        the header
   * @return the response entity
   */
  static <X> ResponseEntity<X> wrapOrNotFound(
    Optional<X> maybeResponse,
    HttpHeaders header
  ) {
    return maybeResponse
      .map(ResponseEntity.ok().headers(header)::body)
      .orElseThrow(ResponseUtil::getNotFoundResponseStatusException);
  }

  private static ResponseStatusException getNotFoundResponseStatusException() {
    return new ResponseStatusException(HttpStatus.NOT_FOUND);
  }
}
