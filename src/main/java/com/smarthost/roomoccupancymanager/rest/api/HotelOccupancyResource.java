/*
 * Copyright (c) 2021 Fadi William Ghali Abdelmessih<fadi.william.ghali@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies
 * or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

package com.smarthost.roomoccupancymanager.rest.api;

import com.smarthost.roomoccupancymanager.rest.util.HeaderUtil;
import com.smarthost.roomoccupancymanager.rest.util.PaginationUtil;
import com.smarthost.roomoccupancymanager.rest.util.ResponseUtil;
import com.smarthost.roomoccupancymanager.service.HotelOccupancyService;
import com.smarthost.roomoccupancymanager.service.dto.HotelOccupancyDTO;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.smarthost.roomoccupancymanager.domain.HotelOccupancy}.
 *
 * @author Fadi William Ghali Abdelmessih
 */
@RestController
@RequestMapping("/api")
@Tag(name = "HotelOccupancyResource")
public class HotelOccupancyResource {

  private final Logger log = LoggerFactory.getLogger(
    HotelOccupancyResource.class
  );

  private static final String ENTITY_NAME = "hotelOccupancy";

  @Value("${spring.application.name}")
  private String applicationName;

  private final HotelOccupancyService hotelOccupancyService;

  /**
   * Instantiates a new hotel occupancy resource.
   *
   * @param hotelOccupancyService the hotel occupancy service
   */
  public HotelOccupancyResource(HotelOccupancyService hotelOccupancyService) {
    this.hotelOccupancyService = hotelOccupancyService;
  }

  /**
   * {@code POST  /hotel-occupancies} : Create or update a hotelOccupancy based on the properties.
   *
   * @param hotelOccupancyDTO the hotelOccupancyDTO to create.
   * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new hotelOccupancyDTO, or with status {@code 400 (Bad Request)} if the hotelOccupancy has already an ID.
   * @throws URISyntaxException if the Location URI syntax is incorrect.
   */
  @PostMapping("/hotel-occupancies")
  public ResponseEntity<HotelOccupancyDTO> createHotelOccupancy(
    @Valid @RequestBody HotelOccupancyDTO hotelOccupancyDTO
  ) throws URISyntaxException {
    log.debug("REST request to save HotelOccupancy : {}", hotelOccupancyDTO);
    if (hotelOccupancyDTO.getId() != null) {
      return ResponseEntity
        .badRequest()
        .header(
          "Failure",
          "A new hotelOccupancy (Or the hotelOccupancy to be updated) cannot already have an ID"
        )
        .body(null);
    }
    HotelOccupancyDTO result = hotelOccupancyService.save(hotelOccupancyDTO);
    return ResponseEntity
      .ok()
      .headers(
        HeaderUtil.createOrUpdateEntityCreationAlert(
          applicationName,
          false,
          ENTITY_NAME,
          result.getId().toString()
        )
      )
      .body(result);
  }

  /**
   * {@code GET  /hotel-occupancies/:hotelId} : get all the hotelOccupancies by hotelId.
   *
   * @param pageable the pagination information.
   * @param hotelId  the hotel id
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of hotelOccupancies in body.
   */
  @GetMapping("/hotel-occupancies/{hotelId}")
  public ResponseEntity<List<HotelOccupancyDTO>> getAllHotelOccupancies(
    Pageable pageable,
    @PathVariable Long hotelId
  ) {
    log.debug(
      "REST request to get a page of HotelOccupancies based on the hotelId: {}",
      hotelId
    );
    Page<HotelOccupancyDTO> page = hotelOccupancyService.findAllByHotelId(
      hotelId,
      pageable
    );
    HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(
      ServletUriComponentsBuilder.fromCurrentRequest(),
      page
    );
    return ResponseEntity.ok().headers(headers).body(page.getContent());
  }

  /**
   * {@code GET  /hotel-occupancies/:date/:hotelId} : get the hotelOccupancy.
   *
   * @param date    the date
   * @param hotelId the hotel id
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the hotelOccupancyDTO, or with status {@code 404 (Not Found)}.
   */
  @GetMapping("/hotel-occupancies/{date}/{hotelId}")
  public ResponseEntity<HotelOccupancyDTO> getHotelOccupancy(
    @PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date,
    @PathVariable Long hotelId
  ) {
    log.debug(
      "REST request to get HotelOccupancy based on the date: {} & hotelId: {}",
      date,
      hotelId
    );
    Optional<HotelOccupancyDTO> hotelOccupancyDTO = hotelOccupancyService.findOneByDateAndHotelId(
      date,
      hotelId
    );
    return ResponseUtil.wrapOrNotFound(hotelOccupancyDTO);
  }

  /**
   * {@code DELETE  /hotel-occupancies/:date/:hotelId} : delete the hotelOccupancy.
   *
   * @param id the id of the hotelOccupancyDTO to delete.
   * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
   */
  @DeleteMapping("/hotel-occupancies/{date}/{hotelId}")
  public ResponseEntity<Void> deleteHotelOccupancy(
    @PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date,
    @PathVariable Long hotelId
  ) {
    log.debug(
      "REST request to delete HotelOccupancy based on the date: {} & hotelId: {}",
      date,
      hotelId
    );
    Optional<HotelOccupancyDTO> hotelOccupancyDTO = hotelOccupancyService.findOneByDateAndHotelId(
      date,
      hotelId
    );

    if (hotelOccupancyDTO.isPresent()) {
      hotelOccupancyService.deleteOneByDateAndHotelId(date, hotelId);
      return ResponseEntity
        .noContent()
        .headers(
          HeaderUtil.createEntityDeletionAlert(
            applicationName,
            false,
            ENTITY_NAME,
            hotelOccupancyDTO.get().getId().toString()
          )
        )
        .build();
    } else {
      return ResponseEntity
        .badRequest()
        .header(
          "Failure",
          "There is no hotelOccupancy that matches the arguments passed"
        )
        .body(null);
    }
  }
}
