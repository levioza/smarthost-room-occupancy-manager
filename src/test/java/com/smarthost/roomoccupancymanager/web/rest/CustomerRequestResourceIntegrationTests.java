/*
 * Copyright (c) 2021 Fadi William Ghali Abdelmessih<fadi.william.ghali@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies
 * or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

package com.smarthost.roomoccupancymanager.web.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.smarthost.roomoccupancymanager.RoomOccupancyManagerApplication;
import com.smarthost.roomoccupancymanager.domain.Customer;
import com.smarthost.roomoccupancymanager.domain.CustomerRequest;
import com.smarthost.roomoccupancymanager.domain.Hotel;
import com.smarthost.roomoccupancymanager.repository.CustomerRepository;
import com.smarthost.roomoccupancymanager.repository.CustomerRequestRepository;
import com.smarthost.roomoccupancymanager.repository.HotelRepository;
import com.smarthost.roomoccupancymanager.service.dto.CustomerRequestDTO;
import com.smarthost.roomoccupancymanager.service.mapper.CustomerRequestMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link CustomerRequestResource} REST controller.
 *
 * @author Fadi William Ghali Abdelmessih
 */
@SpringBootTest(classes = RoomOccupancyManagerApplication.class)
@AutoConfigureMockMvc
class CustomerRequestResourceIntegrationTests {

  private static final LocalDate DATE = LocalDate.now(ZoneId.systemDefault());

  private static final Long CUSTOMER_ID = 1L;

  private static final Long HOTEL_ID = 1L;

  private static final BigDecimal DEFAULT_AMOUNT_CUSTOMER_WILLING_TO_PAY = new BigDecimal(
    25
  );
  private static final BigDecimal UPDATED_AMOUNT_CUSTOMER_WILLING_TO_PAY = new BigDecimal(
    100
  );

  @Autowired
  private CustomerRequestRepository customerRequestRepository;

  @Autowired
  private CustomerRepository customerRepository;

  @Autowired
  private HotelRepository hotelRepository;

  @Autowired
  private CustomerRequestMapper customerRequestMapper;

  @Autowired
  private EntityManager em;

  @Autowired
  private ObjectMapper objectMapper;

  @Autowired
  private MockMvc restCustomerRequestMockMvc;

  private CustomerRequest customerRequest;

  /**
   * Create an entity for this test.
   *
   * @param em the em
   * @return the customer request
   */
  public static CustomerRequest createEntity(EntityManager em) {
    CustomerRequest customerRequest = new CustomerRequest()
      .date(DATE)
      .customer(em.find(Customer.class, CUSTOMER_ID))
      .hotel(em.find(Hotel.class, HOTEL_ID))
      .amountCustomerWillingToPay(DEFAULT_AMOUNT_CUSTOMER_WILLING_TO_PAY);
    return customerRequest;
  }

  /**
   * Init test.
   */
  @BeforeEach
  public void initTest() {
    customerRequest = createEntity(em);
  }

  /**
   * Create customer request.
   *
   * @throws Exception the exception
   */
  @Test
  @Transactional
  void createCustomerRequest() throws Exception {
    int databaseSizeBeforeCreate = customerRequestRepository.findAll().size();

    // Create the CustomerRequest.
    CustomerRequestDTO customerRequestDTO = customerRequestMapper.toDto(
      customerRequest
    );
    restCustomerRequestMockMvc
      .perform(
        post("/api/customer-requests")
          .contentType(MediaType.APPLICATION_JSON)
          .content(TestUtil.convertObjectToJsonBytes(customerRequestDTO))
      )
      .andExpect(status().isOk());

    // Validate the CustomerRequest in the database.
    List<CustomerRequest> customerRequestList = customerRequestRepository.findAll();
    assertThat(customerRequestList).hasSize(databaseSizeBeforeCreate + 1);
    CustomerRequest testCustomerRequest = customerRequestList.get(
      customerRequestList.size() - 1
    );
    assertThat(testCustomerRequest.getDate()).isEqualTo(DATE);
    assertThat(testCustomerRequest.getCustomer().getId())
      .isEqualTo(CUSTOMER_ID);
    assertThat(testCustomerRequest.getHotel().getId()).isEqualTo(HOTEL_ID);
    assertThat(testCustomerRequest.getAmountCustomerWillingToPay())
      .isEqualTo(DEFAULT_AMOUNT_CUSTOMER_WILLING_TO_PAY);
  }

  /**
   * Check date is required.
   *
   * @throws Exception the exception
   */
  @Test
  @Transactional
  void checkDateIsRequired() throws Exception {
    int databaseSizeBeforeTest = customerRequestRepository.findAll().size();

    // Set the field to null.
    customerRequest.setDate(null);

    // Create the CustomerRequest, which fails.
    CustomerRequestDTO customerRequestDTO = customerRequestMapper.toDto(
      customerRequest
    );

    restCustomerRequestMockMvc
      .perform(
        post("/api/customer-requests")
          .contentType(MediaType.APPLICATION_JSON)
          .content(TestUtil.convertObjectToJsonBytes(customerRequestDTO))
      )
      .andExpect(status().isBadRequest());

    List<CustomerRequest> customerRequestList = customerRequestRepository.findAll();
    assertThat(customerRequestList).hasSize(databaseSizeBeforeTest);
  }

  /**
   * Check amount customer willing to pay is required.
   *
   * @throws Exception the exception
   */
  @Test
  @Transactional
  void checkAmountCustomerWillingToPayIsRequired() throws Exception {
    int databaseSizeBeforeTest = customerRequestRepository.findAll().size();

    // Set the field to null.
    customerRequest.setAmountCustomerWillingToPay(null);

    // Create the CustomerRequest, which fails.
    CustomerRequestDTO customerRequestDTO = customerRequestMapper.toDto(
      customerRequest
    );

    restCustomerRequestMockMvc
      .perform(
        post("/api/customer-requests")
          .contentType(MediaType.APPLICATION_JSON)
          .content(TestUtil.convertObjectToJsonBytes(customerRequestDTO))
      )
      .andExpect(status().isBadRequest());

    List<CustomerRequest> customerRequestList = customerRequestRepository.findAll();
    assertThat(customerRequestList).hasSize(databaseSizeBeforeTest);
  }

  /**
   * Check customer id is required.
   *
   * @throws Exception the exception
   */
  @Test
  @Transactional
  void checkCustomerIdIsRequired() throws Exception {
    int databaseSizeBeforeTest = customerRequestRepository.findAll().size();

    // Set the field to null.
    customerRequest.setCustomer(null);

    // Create the CustomerRequest, which fails.
    CustomerRequestDTO customerRequestDTO = customerRequestMapper.toDto(
      customerRequest
    );

    restCustomerRequestMockMvc
      .perform(
        post("/api/customer-requests")
          .contentType(MediaType.APPLICATION_JSON)
          .content(TestUtil.convertObjectToJsonBytes(customerRequestDTO))
      )
      .andExpect(status().isBadRequest());

    List<CustomerRequest> customerRequestList = customerRequestRepository.findAll();
    assertThat(customerRequestList).hasSize(databaseSizeBeforeTest);
  }

  /**
   * Check hotel id is required.
   *
   * @throws Exception the exception
   */
  @Test
  @Transactional
  void checkHotelIdIsRequired() throws Exception {
    int databaseSizeBeforeTest = customerRequestRepository.findAll().size();

    // Set the field to null.
    customerRequest.setHotel(null);

    // Create the CustomerRequest, which fails.
    CustomerRequestDTO customerRequestDTO = customerRequestMapper.toDto(
      customerRequest
    );

    restCustomerRequestMockMvc
      .perform(
        post("/api/customer-requests")
          .contentType(MediaType.APPLICATION_JSON)
          .content(TestUtil.convertObjectToJsonBytes(customerRequestDTO))
      )
      .andExpect(status().isBadRequest());

    List<CustomerRequest> customerRequestList = customerRequestRepository.findAll();
    assertThat(customerRequestList).hasSize(databaseSizeBeforeTest);
  }

  /**
   * Gets all customer requests based on the customer id.
   *
   * @throws Exception the exception
   */
  @Test
  @Transactional
  void getAllCustomerRequestsByCustomerId() throws Exception {
    // Get all the customerRequestList based on the customer id.
    MvcResult result = restCustomerRequestMockMvc
      .perform(get("/api/customer-requests/1?sort=id,desc"))
      .andExpect(status().isOk())
      .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
      .andReturn();

    CustomerRequestDTO[] customerRequestDTOArray = objectMapper.readValue(
      result.getResponse().getContentAsString(),
      CustomerRequestDTO[].class
    );
    assertThat(customerRequestDTOArray[0].getCustomerId())
      .isEqualTo(CUSTOMER_ID);

    Set<Long> customerIdSet = new HashSet<>();

    Arrays
      .asList(customerRequestDTOArray)
      .forEach(
        customerRequestDTO -> {
          customerIdSet.add(customerRequestDTO.getCustomerId());
        }
      );

    assertThat(customerIdSet.size()).isEqualTo(1);
  }

  /**
   * Gets all customer requests.
   *
   * @throws Exception the exception
   */
  @Test
  @Transactional
  void getAllCustomerRequests() throws Exception {
    // Get all the customerRequestList.
    MvcResult result = restCustomerRequestMockMvc
      .perform(get("/api/customer-requests/1/2021-02-21?sort=id,desc"))
      .andExpect(status().isOk())
      .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
      .andReturn();

    CustomerRequestDTO[] customerRequestDTOArray = objectMapper.readValue(
      result.getResponse().getContentAsString(),
      CustomerRequestDTO[].class
    );
    assertThat(customerRequestDTOArray[0].getCustomerId())
      .isEqualTo(CUSTOMER_ID);
    assertThat(customerRequestDTOArray[0].getDate())
      .isEqualTo(LocalDate.parse("2021-02-21"));

    Set<Long> customerIdSet = new HashSet<>();
    Set<LocalDate> localDateSet = new HashSet<>();

    Arrays
      .asList(customerRequestDTOArray)
      .forEach(
        customerRequestDTO -> {
          customerIdSet.add(customerRequestDTO.getCustomerId());
          localDateSet.add(customerRequestDTO.getDate());
        }
      );

    assertThat(customerIdSet.size()).isEqualTo(1);
    assertThat(localDateSet.size()).isEqualTo(1);
  }

  /**
   * Gets customer request by date, customer id and hotel id.
   *
   * @throws Exception the exception
   */
  @Test
  @Transactional
  void getCustomerRequest() throws Exception {
    // Initialize the database
    customerRequestRepository.saveAndFlush(customerRequest);

    // Get the customerRequest by the date, customer id and hotel id.
    restCustomerRequestMockMvc
      .perform(
        get(
          "/api/customer-requests/{date}/{customerId}/{hotelId}",
          customerRequest.getDate(),
          customerRequest.getCustomer().getId(),
          customerRequest.getHotel().getId()
        )
      )
      .andExpect(status().isOk())
      .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
      .andExpect(
        jsonPath("$.amountCustomerWillingToPay")
          .value(DEFAULT_AMOUNT_CUSTOMER_WILLING_TO_PAY.intValue())
      );
  }

  /**
   * Gets non existing customer request.
   *
   * @throws Exception the exception
   */
  @Test
  @Transactional
  void getNonExistingCustomerRequest() throws Exception {
    // Get the customerRequest that doesn't exist.
    restCustomerRequestMockMvc
      .perform(
        get(
          "/api/customer-requests/{date}/{customerId}/{hotelId}",
          DATE,
          189,
          189
        )
      )
      .andExpect(status().isNotFound());
  }

  /**
   * Create or update customer request with existing properties.
   *
   * @throws Exception the exception
   */
  @Test
  @Transactional
  void createOrUpdateCustomerRequestWithExistingProperties() throws Exception {
    // Save the customer request.
    customerRequestRepository.saveAndFlush(customerRequest);

    // Get the customerRequests database records size.
    int databaseSizeBeforeCreate = customerRequestRepository.findAll().size();

    // Disconnect from session so that the updates on customerRequest are not directly saved in db.
    em.detach(customerRequest);

    // Update the customer request.
    customerRequest.setId(null);
    customerRequest.setAmountCustomerWillingToPay(
      UPDATED_AMOUNT_CUSTOMER_WILLING_TO_PAY
    );

    CustomerRequestDTO customerRequestDTO = customerRequestMapper.toDto(
      customerRequest
    );

    // An entity with existing properties will be upserted.
    restCustomerRequestMockMvc
      .perform(
        post("/api/customer-requests")
          .contentType(MediaType.APPLICATION_JSON)
          .content(TestUtil.convertObjectToJsonBytes(customerRequestDTO))
      )
      .andExpect(status().isOk());

    // Validate the number of customer requests in the database didn't change.
    List<CustomerRequest> customerRequestList = customerRequestRepository.findAll();
    assertThat(customerRequestList).hasSize(databaseSizeBeforeCreate);

    // Make sure the record has been updated in the database.
    CustomerRequest testCustomerRequest = customerRequestRepository
      .findOneByDateAndCustomer_idAndHotel_id(DATE, CUSTOMER_ID, HOTEL_ID)
      .get();
    assertThat(testCustomerRequest.getAmountCustomerWillingToPay())
      .isEqualTo(UPDATED_AMOUNT_CUSTOMER_WILLING_TO_PAY);
  }

  /**
   * Delete customer request by date, customer id, hotel id.
   *
   * @throws Exception the exception
   */
  @Test
  @Transactional
  void deleteCustomerRequest() throws Exception {
    // Initialize the database
    customerRequestRepository.saveAndFlush(customerRequest);

    int databaseSizeBeforeDelete = customerRequestRepository.findAll().size();

    // Delete the customerRequest.
    restCustomerRequestMockMvc
      .perform(
        delete(
          "/api/customer-requests/{date}/{customerId}/{hotelId}",
          customerRequest.getDate(),
          customerRequest.getCustomer().getId(),
          customerRequest.getHotel().getId()
        )
          .accept(MediaType.APPLICATION_JSON)
      )
      .andExpect(status().isNoContent());

    // Validate that the database contains one less item.
    List<CustomerRequest> customerRequestList = customerRequestRepository.findAll();
    assertThat(customerRequestList).hasSize(databaseSizeBeforeDelete - 1);
  }
}
