/*
 * Copyright (c) 2021 Fadi William Ghali Abdelmessih<fadi.william.ghali@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies
 * or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

package com.smarthost.roomoccupancymanager.rest.api;

import com.smarthost.roomoccupancymanager.service.HotelOccupancyEstimatorService;
import com.smarthost.roomoccupancymanager.service.dto.HotelOccupancyRoomReservationSummaryDTO;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.List;

/**
 * The hotel occupancy estimator resource.
 *
 * @author Fadi William Ghali Abdelmessih
 */
@RestController
@RequestMapping("/api")
@Tag(name = "HotelOccupancyEstimatorResource")
public class HotelOccupancyEstimatorResource {

  private final Logger log = LoggerFactory.getLogger(
    HotelOccupancyEstimatorResource.class
  );

  private final HotelOccupancyEstimatorService hotelOccupancyEstimatorService;

  /**
   * Instantiates a new hotel occupancy estimator resource.
   *
   * @param hotelOccupancyEstimatorService the hotel occupancy estimator service
   */
  public HotelOccupancyEstimatorResource(
    HotelOccupancyEstimatorService hotelOccupancyEstimatorService
  ) {
    this.hotelOccupancyEstimatorService = hotelOccupancyEstimatorService;
  }

  /**
   * {@code GET  /hotel-occupancy-estimator/:requestedDate/:hotelId/:upgradable} : get the hotel occupancy estimation based on the requested date and the hotel id.
   *
   * @param requestedDate the date
   * @param hotelId       the hotel id
   * @param upgradable    the upgradable
   * @return the hotel occupancy estimation
   */
  @GetMapping(
    "/hotel-occupancy-estimator/{requestedDate}/{hotelId}/{upgradable}"
  )
  public ResponseEntity<List<HotelOccupancyRoomReservationSummaryDTO>> getHotelOccupancyEstimation(
    @PathVariable @DateTimeFormat(
      pattern = "yyyy-MM-dd"
    ) LocalDate requestedDate,
    @PathVariable Long hotelId,
    @PathVariable Boolean upgradable
  ) {
    log.debug(
      "REST request to get hotel occupancy estimation based on the date: {} & hotelId: {} with upgrade feature is set to: {}",
      requestedDate,
      hotelId,
      upgradable
    );
    List<HotelOccupancyRoomReservationSummaryDTO> hotelOccupancyRoomReservationSummaryDTOList = hotelOccupancyEstimatorService.estimate(
      hotelId,
      requestedDate,
      upgradable
    );
    return ResponseEntity.ok(hotelOccupancyRoomReservationSummaryDTOList);
  }
}
