/*
 * Copyright (c) 2021 Fadi William Ghali Abdelmessih<fadi.william.ghali@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies
 * or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

package com.smarthost.roomoccupancymanager.config.database;

import com.smarthost.roomoccupancymanager.config.profile.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.sql.SQLException;

/**
 * Database configuration.
 *
 * @author Fadi William Ghali Abdelmessih
 */
@Configuration
@EnableJpaRepositories("com.smarthost.roomoccupancymanager.repository")
@EnableTransactionManagement
public class DatabaseConfiguration {

  private final Logger log = LoggerFactory.getLogger(
    DatabaseConfiguration.class
  );

  private final Environment env;

  public DatabaseConfiguration(Environment env) {
    this.env = env;
  }

  /**
   * Open the TCP port for the H2 database, so it is available remotely.
   *
   * @return the H2 database TCP server.
   * @throws SQLException if the server failed to start.
   */
  @Bean(initMethod = "start", destroyMethod = "stop")
  @Profile(Constants.SPRING_PROFILE_DEVELOPMENT)
  public Object h2TCPServer() throws SQLException, H2InstantiationException {
    String port = getValidPortForH2();
    log.debug("H2 database is available on port {}", port);
    return H2ConfigurationHelper.createServer(port);
  }

  private String getValidPortForH2() {
    int port = Integer.parseInt(env.getProperty("server.port"));
    if (port < 10000) {
      port = 10000 + port;
    } else {
      if (port < 63536) {
        port = port + 2000;
      } else {
        port = port - 2000;
      }
    }
    return String.valueOf(port);
  }
}
