/*
 * Copyright (c) 2021 Fadi William Ghali Abdelmessih<fadi.william.ghali@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies
 * or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

package com.smarthost.roomoccupancymanager.service.impl;

import com.smarthost.roomoccupancymanager.domain.CustomerRequest;
import com.smarthost.roomoccupancymanager.repository.CustomerRequestRepository;
import com.smarthost.roomoccupancymanager.service.CustomerRequestService;
import com.smarthost.roomoccupancymanager.service.dto.CustomerRequestDTO;
import com.smarthost.roomoccupancymanager.service.mapper.CustomerRequestMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.Optional;

/**
 * Service Implementation for managing {@link CustomerRequest}.
 *
 * @author Fadi william Ghali Abdelmessih
 */
@Service
@Transactional
public class CustomerRequestServiceImpl implements CustomerRequestService {

  private final Logger log = LoggerFactory.getLogger(
    CustomerRequestServiceImpl.class
  );

  private final CustomerRequestRepository customerRequestRepository;

  private final CustomerRequestMapper customerRequestMapper;

  /**
   * Instantiates a new customer request service.
   *
   * @param customerRequestRepository the customer request repository
   * @param customerRequestMapper     the customer request mapper
   */
  public CustomerRequestServiceImpl(
    CustomerRequestRepository customerRequestRepository,
    CustomerRequestMapper customerRequestMapper
  ) {
    this.customerRequestRepository = customerRequestRepository;
    this.customerRequestMapper = customerRequestMapper;
  }

  @Override
  public CustomerRequestDTO save(CustomerRequestDTO customerRequestDTO) {
    log.debug("Request to save CustomerRequest : {}", customerRequestDTO);
    CustomerRequest customerRequest = customerRequestMapper.toEntity(
      customerRequestDTO
    );

    // Check if there's a customer request already for the same customer at the same hotel on the same date.
    Optional<CustomerRequest> customerRequestThatAlreadyExists = customerRequestRepository.findOneByDateAndCustomer_idAndHotel_id(
      customerRequest.getDate(),
      customerRequest.getCustomer().getId(),
      customerRequest.getHotel().getId()
    );

    if (customerRequestThatAlreadyExists.isPresent()) {
      // Set the id of the customer request object to perform an upsert.
      customerRequest.setId(customerRequestThatAlreadyExists.get().getId());
    }

    customerRequest = customerRequestRepository.save(customerRequest);
    return customerRequestMapper.toDto(customerRequest);
  }

  @Override
  public Page<CustomerRequestDTO> findAllByCustomerId(
    Long customerId,
    Pageable pageable
  ) {
    log.debug(
      "Request to get all CustomerRequests based on the customerId: {}",
      customerId
    );
    return customerRequestRepository
      .findAllByCustomer_id(customerId, pageable)
      .map(customerRequestMapper::toDto);
  }

  @Override
  @Transactional(readOnly = true)
  public Page<CustomerRequestDTO> findAllByDateAndCustomerId(
    LocalDate date,
    Long customerId,
    Pageable pageable
  ) {
    log.debug(
      "Request to get all CustomerRequests based on the date: {} & customerId: {}",
      date,
      customerId
    );
    return customerRequestRepository
      .findAllByDateAndCustomer_id(date, customerId, pageable)
      .map(customerRequestMapper::toDto);
  }

  @Override
  @Transactional(readOnly = true)
  public Optional<CustomerRequestDTO> findOneByDateAndCustomerIdAndHotelId(
    LocalDate date,
    Long customerId,
    Long hotelId
  ) {
    log.debug(
      "Request to get CustomerRequest based on the date: {}, customerId: {} & hotelId: {}",
      date,
      customerId,
      hotelId
    );
    return customerRequestRepository
      .findOneByDateAndCustomer_idAndHotel_id(date, customerId, hotelId)
      .map(customerRequestMapper::toDto);
  }

  @Override
  public void deleteOneByDateAndCustomerIdAndHotelId(
    LocalDate date,
    Long customerId,
    Long hotelId
  ) {
    log.debug(
      "Request to delete CustomerRequest based on the date: {}, customerId: {} & hotelId: {}",
      date,
      customerId,
      hotelId
    );
    customerRequestRepository.deleteOneByDateAndCustomer_idAndHotel_id(
      date,
      customerId,
      hotelId
    );
  }
}
