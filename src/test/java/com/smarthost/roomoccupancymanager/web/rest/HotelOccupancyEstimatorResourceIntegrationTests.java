/*
 * Copyright (c) 2021 Fadi William Ghali Abdelmessih<fadi.william.ghali@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies
 * or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

package com.smarthost.roomoccupancymanager.web.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.smarthost.roomoccupancymanager.RoomOccupancyManagerApplication;
import com.smarthost.roomoccupancymanager.domain.Customer;
import com.smarthost.roomoccupancymanager.domain.CustomerRequest;
import com.smarthost.roomoccupancymanager.domain.Hotel;
import com.smarthost.roomoccupancymanager.domain.HotelOccupancy;
import com.smarthost.roomoccupancymanager.repository.CustomerRequestRepository;
import com.smarthost.roomoccupancymanager.repository.HotelOccupancyRepository;
import com.smarthost.roomoccupancymanager.rest.errors.ErrorConstants;
import com.smarthost.roomoccupancymanager.rest.errors.ErrorDTO;
import com.smarthost.roomoccupancymanager.service.HotelOccupancyEstimatorService;
import com.smarthost.roomoccupancymanager.service.dto.HotelOccupancyRoomReservationSummaryDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Integration tests for the {@link com.smarthost.roomoccupancymanager.rest.api.HotelOccupancyEstimatorResource} REST controller.
 *
 * @author Fadi William Ghali Abdelmessih
 */
@SpringBootTest(classes = RoomOccupancyManagerApplication.class)
@AutoConfigureMockMvc
class HotelOccupancyEstimatorResourceIntegrationTests {

  private static final LocalDate DATE = LocalDate.parse("2021-03-05");

  private static final Long HOTEL_ID = 1L;

  private static final Long CUSTOMER_ID = 1L;

  @Autowired
  private HotelOccupancyEstimatorService hotelOccupancyEstimatorService;

  @Autowired
  private HotelOccupancyRepository hotelOccupancyRepository;

  @Autowired
  private CustomerRequestRepository customerRequestRepository;

  @Autowired
  private EntityManager em;

  @Autowired
  private ObjectMapper objectMapper;

  @Autowired
  private MockMvc restHotelOccupancyEstimatorMockMvc;

  private CustomerRequest customerRequest;

  private HotelOccupancy hotelOccupancy;

  /**
   * Create customer request customer request.
   *
   * @param em the em
   * @return the customer request
   */
  public static CustomerRequest createCustomerRequest(EntityManager em) {
    CustomerRequest customerRequest = new CustomerRequest()
      .date(DATE)
      .customer(em.find(Customer.class, CUSTOMER_ID))
      .hotel(em.find(Hotel.class, HOTEL_ID))
      .amountCustomerWillingToPay(BigDecimal.valueOf(100));
    return customerRequest;
  }

  /**
   * Create hotel occupancy hotel occupancy.
   *
   * @param em the em
   * @return the hotel occupancy
   */
  public static HotelOccupancy createHotelOccupancy(EntityManager em) {
    HotelOccupancy hotelOccupancy = new HotelOccupancy()
      .date(DATE)
      .hotel(em.find(Hotel.class, HOTEL_ID))
      .numberOfPremiumRooms(4)
      .numberOfEconomyRooms(10);
    return hotelOccupancy;
  }

  /**
   * Init test.
   */
  @BeforeEach
  public void initTest() {
    customerRequest = createCustomerRequest(em);
    hotelOccupancy = createHotelOccupancy(em);
  }

  @Test
  @Transactional
  void getHotelOccupancyEstimationForADateAtHotelWithNoCustomerRequests()
    throws Exception {
    // Save an occupancy for this hotel at the specified date.
    hotelOccupancyRepository.saveAndFlush(hotelOccupancy);

    // Make sure that you get an error clarifying that there's no customer requests for this date at the specified hotel.
    MvcResult result = restHotelOccupancyEstimatorMockMvc
      .perform(get("/api/hotel-occupancy-estimator/2021-03-05/1/true"))
      .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
      .andExpect(status().isBadRequest())
      .andReturn();

    ErrorDTO errorDTO = objectMapper.readValue(
      result.getResponse().getContentAsString(),
      ErrorDTO.class
    );
    assertThat(errorDTO.getMessage())
      .isEqualTo(
        ErrorConstants.ERR_NO_CUSTOMER_REQUEST_FOR_THIS_DATE_AT_THIS_HOTEL_EXCEPTION
      );
    assertThat(errorDTO.getDescription())
      .isEqualTo(
        "No customer requests for this date: " +
        DATE +
        " at this hotel: " +
        HOTEL_ID
      );
  }

  @Test
  @Transactional
  void getHotelOccupancyEstimationForADateAtHotelWithNoHotelOccupancyDefined()
    throws Exception {
    // Save an occupancy for this hotel at the specified date.
    customerRequestRepository.saveAndFlush(customerRequest);

    // Make sure that you get an error clarifying that there's no hotel occupancy defined for this date at the specified hotel.
    MvcResult result = restHotelOccupancyEstimatorMockMvc
      .perform(get("/api/hotel-occupancy-estimator/2021-03-05/1/true"))
      .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
      .andExpect(status().isBadRequest())
      .andReturn();

    ErrorDTO errorDTO = objectMapper.readValue(
      result.getResponse().getContentAsString(),
      ErrorDTO.class
    );
    assertThat(errorDTO.getMessage())
      .isEqualTo(ErrorConstants.ERR_NO_HOTEL_OCCUPANCY_FOR_THIS_DATE_EXCEPTION);
    assertThat(errorDTO.getDescription())
      .isEqualTo(
        "No hotel occupancy for this date: " +
        DATE +
        " at this hotel: " +
        HOTEL_ID
      );
  }

  @Test
  @Transactional
  void getHotelOccupancyEstimationTest1() throws Exception {
    // Perform an http request for hotel number 1 and date 2021-02-20.
    MvcResult result = restHotelOccupancyEstimatorMockMvc
      .perform(get("/api/hotel-occupancy-estimator/2021-02-20/1/true"))
      .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
      .andExpect(status().isOk())
      .andReturn();

    HotelOccupancyRoomReservationSummaryDTO[] hotelOccupancyRoomReservationSummaryDTOArray = objectMapper.readValue(
      result.getResponse().getContentAsString(),
      HotelOccupancyRoomReservationSummaryDTO[].class
    );
    assertThat(
      hotelOccupancyRoomReservationSummaryDTOArray[0].getRoomCategory()
    )
      .isEqualTo("premium");
    assertThat(
      hotelOccupancyRoomReservationSummaryDTOArray[0].getNumberOfRoomsReserved()
    )
      .isEqualTo(3);
    assertThat(
      hotelOccupancyRoomReservationSummaryDTOArray[0].getTotalReservationsBudgetToBePaidByTheCustomers()
    )
      .isEqualTo(BigDecimal.valueOf(73800, 2));
    assertThat(
      hotelOccupancyRoomReservationSummaryDTOArray[1].getRoomCategory()
    )
      .isEqualTo("economy");
    assertThat(
      hotelOccupancyRoomReservationSummaryDTOArray[1].getNumberOfRoomsReserved()
    )
      .isEqualTo(3);
    assertThat(
      hotelOccupancyRoomReservationSummaryDTOArray[1].getTotalReservationsBudgetToBePaidByTheCustomers()
    )
      .isEqualTo(BigDecimal.valueOf(16700, 2));
  }

  @Test
  @Transactional
  void getHotelOccupancyEstimationTest2() throws Exception {
    // Perform an http request for hotel number 1 and date 2021-02-21.
    MvcResult result = restHotelOccupancyEstimatorMockMvc
      .perform(get("/api/hotel-occupancy-estimator/2021-02-21/1/false"))
      .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
      .andExpect(status().isOk())
      .andReturn();

    HotelOccupancyRoomReservationSummaryDTO[] hotelOccupancyRoomReservationSummaryDTOArray = objectMapper.readValue(
      result.getResponse().getContentAsString(),
      HotelOccupancyRoomReservationSummaryDTO[].class
    );
    assertThat(
      hotelOccupancyRoomReservationSummaryDTOArray[0].getRoomCategory()
    )
      .isEqualTo("premium");
    assertThat(
      hotelOccupancyRoomReservationSummaryDTOArray[0].getNumberOfRoomsReserved()
    )
      .isEqualTo(6);
    assertThat(
      hotelOccupancyRoomReservationSummaryDTOArray[0].getTotalReservationsBudgetToBePaidByTheCustomers()
    )
      .isEqualTo(BigDecimal.valueOf(105400, 2));
    assertThat(
      hotelOccupancyRoomReservationSummaryDTOArray[1].getRoomCategory()
    )
      .isEqualTo("economy");
    assertThat(
      hotelOccupancyRoomReservationSummaryDTOArray[1].getNumberOfRoomsReserved()
    )
      .isEqualTo(4);
    assertThat(
      hotelOccupancyRoomReservationSummaryDTOArray[1].getTotalReservationsBudgetToBePaidByTheCustomers()
    )
      .isEqualTo(BigDecimal.valueOf(18900, 2));
  }

  @Test
  @Transactional
  void getHotelOccupancyEstimationTest3() throws Exception {
    // Perform an http request for hotel number 1 and date 2021-02-22.
    MvcResult result = restHotelOccupancyEstimatorMockMvc
      .perform(get("/api/hotel-occupancy-estimator/2021-02-22/1/true"))
      .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
      .andExpect(status().isOk())
      .andReturn();

    HotelOccupancyRoomReservationSummaryDTO[] hotelOccupancyRoomReservationSummaryDTOArray = objectMapper.readValue(
      result.getResponse().getContentAsString(),
      HotelOccupancyRoomReservationSummaryDTO[].class
    );
    assertThat(
      hotelOccupancyRoomReservationSummaryDTOArray[0].getRoomCategory()
    )
      .isEqualTo("premium");
    assertThat(
      hotelOccupancyRoomReservationSummaryDTOArray[0].getNumberOfRoomsReserved()
    )
      .isEqualTo(2);
    assertThat(
      hotelOccupancyRoomReservationSummaryDTOArray[0].getTotalReservationsBudgetToBePaidByTheCustomers()
    )
      .isEqualTo(BigDecimal.valueOf(58300, 2));
    assertThat(
      hotelOccupancyRoomReservationSummaryDTOArray[1].getRoomCategory()
    )
      .isEqualTo("economy");
    assertThat(
      hotelOccupancyRoomReservationSummaryDTOArray[1].getNumberOfRoomsReserved()
    )
      .isEqualTo(4);
    assertThat(
      hotelOccupancyRoomReservationSummaryDTOArray[1].getTotalReservationsBudgetToBePaidByTheCustomers()
    )
      .isEqualTo(BigDecimal.valueOf(18900, 2));
  }

  @Test
  @Transactional
  void getHotelOccupancyEstimationTest4() throws Exception {
    // Perform an http request for hotel number 1 and date 2021-02-23.
    MvcResult result = restHotelOccupancyEstimatorMockMvc
      .perform(get("/api/hotel-occupancy-estimator/2021-02-23/1/true"))
      .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
      .andExpect(status().isOk())
      .andReturn();

    HotelOccupancyRoomReservationSummaryDTO[] hotelOccupancyRoomReservationSummaryDTOArray = objectMapper.readValue(
      result.getResponse().getContentAsString(),
      HotelOccupancyRoomReservationSummaryDTO[].class
    );
    assertThat(
      hotelOccupancyRoomReservationSummaryDTOArray[0].getRoomCategory()
    )
      .isEqualTo("premium");
    assertThat(
      hotelOccupancyRoomReservationSummaryDTOArray[0].getNumberOfRoomsReserved()
    )
      .isEqualTo(7);
    assertThat(
      hotelOccupancyRoomReservationSummaryDTOArray[0].getTotalReservationsBudgetToBePaidByTheCustomers()
    )
      .isEqualTo(BigDecimal.valueOf(115300, 2));
    assertThat(
      hotelOccupancyRoomReservationSummaryDTOArray[1].getRoomCategory()
    )
      .isEqualTo("economy");
    assertThat(
      hotelOccupancyRoomReservationSummaryDTOArray[1].getNumberOfRoomsReserved()
    )
      .isEqualTo(1);
    assertThat(
      hotelOccupancyRoomReservationSummaryDTOArray[1].getTotalReservationsBudgetToBePaidByTheCustomers()
    )
      .isEqualTo(BigDecimal.valueOf(4500, 2));
  }
}
