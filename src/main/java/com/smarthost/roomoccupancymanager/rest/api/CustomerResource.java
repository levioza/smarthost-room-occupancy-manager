/*
 * Copyright (c) 2021 Fadi William Ghali Abdelmessih<fadi.william.ghali@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies
 * or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

package com.smarthost.roomoccupancymanager.rest.api;

import com.smarthost.roomoccupancymanager.rest.util.HeaderUtil;
import com.smarthost.roomoccupancymanager.rest.util.PaginationUtil;
import com.smarthost.roomoccupancymanager.rest.util.ResponseUtil;
import com.smarthost.roomoccupancymanager.service.CustomerService;
import com.smarthost.roomoccupancymanager.service.dto.CustomerDTO;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.smarthost.roomoccupancymanager.domain.Customer}.
 *
 * @author Fadi William Ghali Abdelmessih
 */
@RestController
@RequestMapping("/api")
@Tag(name = "CustomerResource")
public class CustomerResource {

  private final Logger log = LoggerFactory.getLogger(CustomerResource.class);

  private static final String ENTITY_NAME = "customer";

  @Value("${spring.application.name}")
  private String applicationName;

  private final CustomerService customerService;

  /**
   * Instantiates a new customer resource.
   *
   * @param customerService the customer service
   */
  public CustomerResource(CustomerService customerService) {
    this.customerService = customerService;
  }

  /**
   * {@code POST  /customers} : Create a new customer.
   *
   * @param customerDTO the customerDTO to create.
   * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new customerDTO, or with status {@code 400 (Bad Request)} if the customer has already an ID.
   * @throws URISyntaxException if the Location URI syntax is incorrect.
   */
  @PostMapping("/customers")
  public ResponseEntity<CustomerDTO> createCustomer(
    @Valid @RequestBody CustomerDTO customerDTO
  ) throws URISyntaxException {
    log.debug("REST request to save Customer : {}", customerDTO);
    if (customerDTO.getId() != null) {
      return ResponseEntity
        .badRequest()
        .header("Failure", "A new customer cannot already have an ID")
        .body(null);
    }
    CustomerDTO result = customerService.save(customerDTO);
    return ResponseEntity
      .created(new URI("/api/customers/" + result.getId()))
      .headers(
        HeaderUtil.createEntityCreationAlert(
          applicationName,
          false,
          ENTITY_NAME,
          result.getId().toString()
        )
      )
      .body(result);
  }

  /**
   * {@code PUT  /customers} : Updates an existing customer.
   *
   * @param customerDTO the customerDTO to update.
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated customerDTO, or with status {@code 400 (Bad Request)} if the customerDTO is not valid, or with status {@code 500 (Internal Server Error)} if the customerDTO couldn't be updated.
   * @throws URISyntaxException if the Location URI syntax is incorrect.
   */
  @PutMapping("/customers")
  public ResponseEntity<CustomerDTO> updateCustomer(
    @Valid @RequestBody CustomerDTO customerDTO
  ) throws URISyntaxException {
    log.debug("REST request to update Customer : {}", customerDTO);
    if (customerDTO.getId() == null) {
      return ResponseEntity
        .badRequest()
        .header("Failure", "The customer id cannot be null")
        .body(null);
    }
    CustomerDTO result = customerService.save(customerDTO);
    return ResponseEntity
      .ok()
      .headers(
        HeaderUtil.createEntityUpdateAlert(
          applicationName,
          false,
          ENTITY_NAME,
          customerDTO.getId().toString()
        )
      )
      .body(result);
  }

  /**
   * {@code GET  /customers} : get all the customers.
   *
   * @param pageable the pagination information.
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of customers in body.
   */
  @GetMapping("/customers")
  public ResponseEntity<List<CustomerDTO>> getAllCustomers(Pageable pageable) {
    log.debug("REST request to get a page of Customers");
    Page<CustomerDTO> page = customerService.findAll(pageable);
    HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(
      ServletUriComponentsBuilder.fromCurrentRequest(),
      page
    );
    return ResponseEntity.ok().headers(headers).body(page.getContent());
  }

  /**
   * {@code GET  /customers/:id} : get the "id" customer.
   *
   * @param id the id of the customerDTO to retrieve.
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the customerDTO, or with status {@code 404 (Not Found)}.
   */
  @GetMapping("/customers/{id}")
  public ResponseEntity<CustomerDTO> getCustomer(@PathVariable Long id) {
    log.debug("REST request to get Customer : {}", id);
    Optional<CustomerDTO> customerDTO = customerService.findOne(id);
    return ResponseUtil.wrapOrNotFound(customerDTO);
  }

  /**
   * {@code DELETE  /customers/:id} : delete the "id" customer.
   *
   * @param id the id of the customerDTO to delete.
   * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
   */
  @DeleteMapping("/customers/{id}")
  public ResponseEntity<Void> deleteCustomer(@PathVariable Long id) {
    log.debug("REST request to delete Customer : {}", id);
    customerService.delete(id);
    return ResponseEntity
      .noContent()
      .headers(
        HeaderUtil.createEntityDeletionAlert(
          applicationName,
          false,
          ENTITY_NAME,
          id.toString()
        )
      )
      .build();
  }
}
