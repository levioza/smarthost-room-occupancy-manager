# Room occupancy Manager - Spring Boot Project

[![Build Status](https://gitlab.com/levioza/smarthost-room-occupancy-manager/badges/master/pipeline.svg)](https://gitlab.com/levioza/smarthost-room-occupancy-manager/-/pipelines)
[![Code Coverage](https://img.shields.io/sonar/https/sonarcloud.io/smarthost-room-occupancy-manager/coverage.svg)](https://sonarcloud.io/dashboard?id=smarthost-room-occupancy-manager)

The goal of this sample project is to be build a room occupancy manager API using Spring Boot for SmartHost based on their requirements.

## The project uses the following technologies:

- [Spring Boot](https://spring.io/projects/spring-boot)
- [Postgres](https://www.postgresql.org)
- [H2](https://www.h2database.com)
- [Liquibase](https://www.liquibase.org)

## Some screenshots of the project

<div align="center">
    <img src="./images/0.png" alt="Swagger API" width="200px" /> 
    <img src="./images/1.png" alt="Swagger API Test 1" width="200px" />
    <img src="./images/2.png" alt="Swagger API Test 2" width="200px" /> 
    <img src="./images/3.png" alt="Swagger API Test 3" width="200px" /> 
    <img src="./images/4.png" alt="Swagger API Test 4" width="200px" />         
</div>

## How to run the project

### Development

During development, the application bootstraps itself. The liquibase changelog runs automatically. So, you could test right away.
To run the application in development, please follow the following steps.

1. Make sure you have mvn installed on your platform by following `https://maven.apache.org/install.html`
2. Export an env variable with the spring boot profile by running `export SPRING_BOOT_PROFILE=dev`
3. Run `mvn spring-boot:run` in the terminal.
4. Now, you can test the apis by visiting `http://localhost:8082/swagger-ui`.

#### Notes

* Make sure so the `JAVA_HOME` environmental variable points to JDK11 or greater.
* If you found any issues or problems while setting up the Java JDK11 or greater. Please contact me at `fadi.william.ghali@gmail.com`.

### Production

1. Make sure you have mvn installed on your platform by following `https://maven.apache.org/install.html`
2. Configure `application-prod.yml` to point to your production grade Postgres SQL (Make sure to use the right db_name, username & password).
3. Run `mvn spring-boot:run`

#### Notes

* It's not recommended to use secrets like the database connection information in plain text in the configuration files. So, I recommend that you set the values in the `application-prod.yml` to point to environmental variables while configuring an appropriate pipeline to deploy the application to a server. Deploying the application as a container to Kubernetes might be an option; in such case, you may set the environmental variables as Kubernetes secrets. 
* For further insights on how to dockerize a spring boot app, perform ci/cd with kubernetes. Please contact me at `fadi.william.ghali@gmail.com`.

### License

```
Copyright (c) 2021 Fadi William Ghali Abdelmessih<fadi.william.ghali@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be included in all copies
or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.
```
