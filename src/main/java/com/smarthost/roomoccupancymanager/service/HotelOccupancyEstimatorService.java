/*
 * Copyright (c) 2021 Fadi William Ghali Abdelmessih<fadi.william.ghali@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies
 * or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

package com.smarthost.roomoccupancymanager.service;

import com.smarthost.roomoccupancymanager.service.dto.HotelOccupancyRoomReservationSummaryDTO;

import java.time.LocalDate;
import java.util.List;

/**
 * The hotel occupancy estimator service interface.
 *
 * @author Fadi William Ghali Abdelmessih
 */
public interface HotelOccupancyEstimatorService {
  /**
   * Estimate gain for a specific hotel at a specific date.
   *
   * @param hotelId    the hotel id
   * @param requestedDate  the local date
   * @param upgradable the option to specify if we can upgrade the customers willing to pay less than 100 euros to book premium rooms.
   * @return the list of hotelOccupancyRoomReservationSummaryDTO
   */
  List<HotelOccupancyRoomReservationSummaryDTO> estimate(
    Long hotelId,
    LocalDate requestedDate,
    Boolean upgradable
  );
}
