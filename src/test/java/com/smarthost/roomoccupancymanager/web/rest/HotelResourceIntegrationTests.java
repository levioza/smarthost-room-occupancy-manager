/*
 * Copyright (c) 2021 Fadi William Ghali Abdelmessih<fadi.william.ghali@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies
 * or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

package com.smarthost.roomoccupancymanager.web.rest;

import com.smarthost.roomoccupancymanager.RoomOccupancyManagerApplication;
import com.smarthost.roomoccupancymanager.domain.Hotel;
import com.smarthost.roomoccupancymanager.repository.HotelRepository;
import com.smarthost.roomoccupancymanager.service.dto.HotelDTO;
import com.smarthost.roomoccupancymanager.service.mapper.HotelMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link HotelResource} REST controller.
 *
 * @author Fadi William Ghali Abdelmessih
 */
@SpringBootTest(classes = RoomOccupancyManagerApplication.class)
@AutoConfigureMockMvc
class HotelResourceIntegrationTests {

  private static final String DEFAULT_NAME = "Four Seasons";
  private static final String UPDATED_NAME = "Four Seasons Grand Hotel";

  @Autowired
  private HotelRepository hotelRepository;

  @Autowired
  private HotelMapper hotelMapper;

  @Autowired
  private EntityManager em;

  @Autowired
  private MockMvc restHotelMockMvc;

  private Hotel hotel;

  /**
   * Create an entity for this test.
   *
   * @return the hotel
   */
  public static Hotel createEntity() {
    Hotel hotel = new Hotel().name(DEFAULT_NAME);
    return hotel;
  }

  /**
   * Create an updated entity for this test.
   *
   * @return the hotel
   */
  public static Hotel createUpdatedEntity() {
    Hotel hotel = new Hotel().name(UPDATED_NAME);
    return hotel;
  }

  /**
   * Init test.
   */
  @BeforeEach
  public void initTest() {
    hotel = createEntity();
  }

  /**
   * Create hotel.
   *
   * @throws Exception the exception
   */
  @Test
  @Transactional
  void createHotel() throws Exception {
    int databaseSizeBeforeCreate = hotelRepository.findAll().size();
    // Create the Hotel.
    HotelDTO hotelDTO = hotelMapper.toDto(hotel);
    restHotelMockMvc
      .perform(
        post("/api/hotels")
          .contentType(MediaType.APPLICATION_JSON)
          .content(TestUtil.convertObjectToJsonBytes(hotelDTO))
      )
      .andExpect(status().isCreated());

    // Validate the Hotel in the database.
    List<Hotel> hotelList = hotelRepository.findAll();
    assertThat(hotelList).hasSize(databaseSizeBeforeCreate + 1);
    Hotel testHotel = hotelList.get(hotelList.size() - 1);
    assertThat(testHotel.getName()).isEqualTo(DEFAULT_NAME);
  }

  /**
   * Create hotel with existing id.
   *
   * @throws Exception the exception
   */
  @Test
  @Transactional
  void createHotelWithExistingId() throws Exception {
    int databaseSizeBeforeCreate = hotelRepository.findAll().size();

    // Create the Hotel with an existing ID.
    hotel.setId(1L);
    HotelDTO hotelDTO = hotelMapper.toDto(hotel);

    // An entity with an existing ID cannot be created, so this API call must fail.
    restHotelMockMvc
      .perform(
        post("/api/hotels")
          .contentType(MediaType.APPLICATION_JSON)
          .content(TestUtil.convertObjectToJsonBytes(hotelDTO))
      )
      .andExpect(status().isBadRequest());

    // Validate the Hotel in the database.
    List<Hotel> hotelList = hotelRepository.findAll();
    assertThat(hotelList).hasSize(databaseSizeBeforeCreate);
  }

  /**
   * Check name is required.
   *
   * @throws Exception the exception
   */
  @Test
  @Transactional
  void checkNameIsRequired() throws Exception {
    int databaseSizeBeforeTest = hotelRepository.findAll().size();
    // Set the field to null.
    hotel.setName(null);

    // Create the Hotel, which fails.
    HotelDTO hotelDTO = hotelMapper.toDto(hotel);

    restHotelMockMvc
      .perform(
        post("/api/hotels")
          .contentType(MediaType.APPLICATION_JSON)
          .content(TestUtil.convertObjectToJsonBytes(hotelDTO))
      )
      .andExpect(status().isBadRequest());

    List<Hotel> hotelList = hotelRepository.findAll();
    assertThat(hotelList).hasSize(databaseSizeBeforeTest);
  }

  /**
   * Gets all hotels.
   *
   * @throws Exception the exception
   */
  @Test
  @Transactional
  void getAllHotels() throws Exception {
    // Initialize the database.
    hotelRepository.saveAndFlush(hotel);

    // Get all the hotelList.
    restHotelMockMvc
      .perform(get("/api/hotels?sort=id,desc"))
      .andExpect(status().isOk())
      .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
      .andExpect(jsonPath("$.[*].id").value(hasItem(hotel.getId().intValue())))
      .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)));
  }

  /**
   * Gets hotel.
   *
   * @throws Exception the exception
   */
  @Test
  @Transactional
  void getHotel() throws Exception {
    // Initialize the database.
    hotelRepository.saveAndFlush(hotel);

    // Get the hotel.
    restHotelMockMvc
      .perform(get("/api/hotels/{id}", hotel.getId()))
      .andExpect(status().isOk())
      .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
      .andExpect(jsonPath("$.id").value(hotel.getId().intValue()))
      .andExpect(jsonPath("$.name").value(DEFAULT_NAME));
  }

  /**
   * Gets non existing hotel.
   *
   * @throws Exception the exception
   */
  @Test
  @Transactional
  void getNonExistingHotel() throws Exception {
    // Get the hotel.
    restHotelMockMvc
      .perform(get("/api/hotels/{id}", Long.MAX_VALUE))
      .andExpect(status().isNotFound());
  }

  /**
   * Update hotel.
   *
   * @throws Exception the exception
   */
  @Test
  @Transactional
  void updateHotel() throws Exception {
    // Initialize the database.
    hotelRepository.saveAndFlush(hotel);

    int databaseSizeBeforeUpdate = hotelRepository.findAll().size();

    // Update the hotel
    Hotel updatedHotel = hotelRepository.findById(hotel.getId()).get();

    // Disconnect from session so that the updates on updatedHotel are not directly saved in db
    em.detach(updatedHotel);

    updatedHotel.name(UPDATED_NAME);
    HotelDTO hotelDTO = hotelMapper.toDto(updatedHotel);

    restHotelMockMvc
      .perform(
        put("/api/hotels")
          .contentType(MediaType.APPLICATION_JSON)
          .content(TestUtil.convertObjectToJsonBytes(hotelDTO))
      )
      .andExpect(status().isOk());

    // Validate the Hotel in the database.
    List<Hotel> hotelList = hotelRepository.findAll();
    assertThat(hotelList).hasSize(databaseSizeBeforeUpdate);
    Hotel testHotel = hotelList.get(hotelList.size() - 1);
    assertThat(testHotel.getName()).isEqualTo(UPDATED_NAME);
  }

  /**
   * Update non existing hotel.
   *
   * @throws Exception the exception
   */
  @Test
  @Transactional
  void updateNonExistingHotel() throws Exception {
    int databaseSizeBeforeUpdate = hotelRepository.findAll().size();

    // Create the Hotel.
    HotelDTO hotelDTO = hotelMapper.toDto(hotel);

    // If the entity doesn't have an ID, it will throw a BadRequest.
    restHotelMockMvc
      .perform(
        put("/api/hotels")
          .contentType(MediaType.APPLICATION_JSON)
          .content(TestUtil.convertObjectToJsonBytes(hotelDTO))
      )
      .andExpect(status().isBadRequest());

    // Validate the Hotel in the database.
    List<Hotel> hotelList = hotelRepository.findAll();
    assertThat(hotelList).hasSize(databaseSizeBeforeUpdate);
  }

  /**
   * Delete hotel.
   *
   * @throws Exception the exception
   */
  @Test
  @Transactional
  void deleteHotel() throws Exception {
    // Initialize the database.
    hotelRepository.saveAndFlush(hotel);

    int databaseSizeBeforeDelete = hotelRepository.findAll().size();

    // Delete the hotel.
    restHotelMockMvc
      .perform(
        delete("/api/hotels/{id}", hotel.getId())
          .accept(MediaType.APPLICATION_JSON)
      )
      .andExpect(status().isNoContent());

    // Validate that the database contains one less item.
    List<Hotel> hotelList = hotelRepository.findAll();
    assertThat(hotelList).hasSize(databaseSizeBeforeDelete - 1);
  }
}
