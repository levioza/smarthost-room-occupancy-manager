/*
 * Copyright (c) 2021 Fadi William Ghali Abdelmessih<fadi.william.ghali@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies
 * or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

package com.smarthost.roomoccupancymanager.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A HotelOccupancy domain entity.
 *
 * @author Fadi William Ghali Abdelmessih
 */
@Entity
@Table(
  name = "hotel_occupancy",
  uniqueConstraints = @UniqueConstraint(columnNames = { "date", "hotel_id" })
)
public class HotelOccupancy implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(
    strategy = GenerationType.SEQUENCE,
    generator = "sequenceGenerator"
  )
  @SequenceGenerator(name = "sequenceGenerator")
  private Long id;

  @NotNull
  @Column(name = "date", nullable = false)
  private LocalDate date;

  @NotNull
  @Column(name = "number_of_premium_rooms", nullable = false)
  private Integer numberOfPremiumRooms;

  @NotNull
  @Column(name = "number_of_economy_rooms", nullable = false)
  private Integer numberOfEconomyRooms;

  @NotNull
  @ManyToOne
  @JsonIgnoreProperties(value = "hotelOccupancies", allowSetters = true)
  private Hotel hotel;

  /**
   * Gets id.
   *
   * @return the id
   */
  public Long getId() {
    return id;
  }

  /**
   * Sets id.
   *
   * @param id the id
   */
  public void setId(Long id) {
    this.id = id;
  }

  /**
   * Gets date.
   *
   * @return the date
   */
  public LocalDate getDate() {
    return date;
  }

  /**
   * Date hotel occupancy.
   *
   * @param date the date
   * @return the hotel occupancy
   */
  public HotelOccupancy date(LocalDate date) {
    this.date = date;
    return this;
  }

  /**
   * Sets date.
   *
   * @param date the date
   */
  public void setDate(LocalDate date) {
    this.date = date;
  }

  /**
   * Gets number of premium rooms.
   *
   * @return the number of premium rooms
   */
  public Integer getNumberOfPremiumRooms() {
    return numberOfPremiumRooms;
  }

  /**
   * Number of premium rooms hotel occupancy.
   *
   * @param numberOfPremiumRooms the number of premium rooms
   * @return the hotel occupancy
   */
  public HotelOccupancy numberOfPremiumRooms(Integer numberOfPremiumRooms) {
    this.numberOfPremiumRooms = numberOfPremiumRooms;
    return this;
  }

  /**
   * Sets number of premium rooms.
   *
   * @param numberOfPremiumRooms the number of premium rooms
   */
  public void setNumberOfPremiumRooms(Integer numberOfPremiumRooms) {
    this.numberOfPremiumRooms = numberOfPremiumRooms;
  }

  /**
   * Gets number of economy rooms.
   *
   * @return the number of economy rooms
   */
  public Integer getNumberOfEconomyRooms() {
    return numberOfEconomyRooms;
  }

  /**
   * Number of economy rooms hotel occupancy.
   *
   * @param numberOfEconomyRooms the number of economy rooms
   * @return the hotel occupancy
   */
  public HotelOccupancy numberOfEconomyRooms(Integer numberOfEconomyRooms) {
    this.numberOfEconomyRooms = numberOfEconomyRooms;
    return this;
  }

  /**
   * Sets number of economy rooms.
   *
   * @param numberOfEconomyRooms the number of economy rooms
   */
  public void setNumberOfEconomyRooms(Integer numberOfEconomyRooms) {
    this.numberOfEconomyRooms = numberOfEconomyRooms;
  }

  /**
   * Gets hotel.
   *
   * @return the hotel
   */
  public Hotel getHotel() {
    return hotel;
  }

  /**
   * Hotel hotel occupancy.
   *
   * @param hotel the hotel
   * @return the hotel occupancy
   */
  public HotelOccupancy hotel(Hotel hotel) {
    this.hotel = hotel;
    return this;
  }

  /**
   * Sets hotel.
   *
   * @param hotel the hotel
   */
  public void setHotel(Hotel hotel) {
    this.hotel = hotel;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof HotelOccupancy)) {
      return false;
    }
    return id != null && id.equals(((HotelOccupancy) o).id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }

  @Override
  public String toString() {
    return (
      "HotelOccupancy{" +
      "id=" +
      getId() +
      ", date='" +
      getDate() +
      "'" +
      ", numberOfPremiumRooms=" +
      getNumberOfPremiumRooms() +
      ", numberOfEconomyRooms=" +
      getNumberOfEconomyRooms() +
      "}"
    );
  }
}
