/*
 * Copyright (c) 2021 Fadi William Ghali Abdelmessih<fadi.william.ghali@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies
 * or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

package com.smarthost.roomoccupancymanager.service.impl;

import com.smarthost.roomoccupancymanager.domain.CustomerRequest;
import com.smarthost.roomoccupancymanager.domain.HotelOccupancy;
import com.smarthost.roomoccupancymanager.repository.CustomerRequestRepository;
import com.smarthost.roomoccupancymanager.repository.HotelOccupancyRepository;
import com.smarthost.roomoccupancymanager.service.HotelOccupancyEstimatorService;
import com.smarthost.roomoccupancymanager.service.dto.HotelOccupancyRoomReservationSummaryDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

/**
 * The hotel occupancy estimator service.
 *
 * @author Fadi William Ghali Abdelmessih
 */
@Service
@Transactional
public class HotelOccupancyEstimatorServiceImpl
  implements HotelOccupancyEstimatorService {

  private final Logger log = LoggerFactory.getLogger(
    HotelOccupancyEstimatorServiceImpl.class
  );

  private final CustomerRequestRepository customerRequestRepository;

  private final HotelOccupancyRepository hotelOccupancyRepository;

  /**
   * Instantiates a new hotel occupancy estimator service.
   *
   * @param customerRequestRepository the customer request repository
   * @param hotelOccupancyRepository  the hotel occupancy repository
   */
  public HotelOccupancyEstimatorServiceImpl(
    CustomerRequestRepository customerRequestRepository,
    HotelOccupancyRepository hotelOccupancyRepository
  ) {
    this.customerRequestRepository = customerRequestRepository;
    this.hotelOccupancyRepository = hotelOccupancyRepository;
  }

  @Override
  public List<HotelOccupancyRoomReservationSummaryDTO> estimate(
    Long hotelId,
    LocalDate requestedDate,
    Boolean upgradable
  ) {
    log.debug(
      "Estimate hotel gain based on the customer requests for hotel: {} at {}",
      hotelId,
      requestedDate
    );

    // Get all the customer requests for this hotel at that specific date.
    List<CustomerRequest> customerRequests = customerRequestRepository.findAllByDateAndHotel_id(
      requestedDate,
      hotelId
    );

    if (customerRequests.isEmpty()) {
      throw new NoCustomerRequestForThisDateAtThisHotelException(
        "No customer requests for this date: " +
        requestedDate +
        " at this hotel: " +
        hotelId
      );
    }

    // Get the hotel occupancy for this hotel at that specific date.
    Optional<HotelOccupancy> hotelOccupancy = hotelOccupancyRepository.findOneByDateAndHotel_id(
      requestedDate,
      hotelId
    );

    if (hotelOccupancy.isEmpty()) {
      throw new NoHotelOccupancyForThisDateException(
        "No hotel occupancy for this date: " +
        requestedDate +
        " at this hotel: " +
        hotelId
      );
    }

    // Execute the algorithm.
    return execute(customerRequests, hotelOccupancy.get(), upgradable);
  }

  /**
   * Execute the algorithm.
   *
   * @param customerRequests the customer requests to be processed for a single date for a single hotel.
   * @param hotelOccupancy   the hotel occupancy provided by the hotel.
   * @param upgradable       the option to specify if we can upgrade the customers willing to pay less than 100 euros to book premium rooms.
   * @return the list of the HotelOccupancyRoomReservationSummaryDTO objects.
   */
  // It's true that it's a little complex... But, tools aren't good to understand this kind of stuff.
  @java.lang.SuppressWarnings("java:S3776")
  private static List<HotelOccupancyRoomReservationSummaryDTO> execute(
    List<CustomerRequest> customerRequests,
    HotelOccupancy hotelOccupancy,
    Boolean upgradable
  ) {
    List<HotelOccupancyRoomReservationSummaryDTO> hotelOccupancyRoomReservationSummaryDTOList = new ArrayList<>();

    // Filter by the customers willing to pay less than 100 and sort them.
    List<CustomerRequest> cRsForCustomersWillingToPayLessThan100Euros = customerRequests
      .stream()
      .filter(
        c ->
          c.getAmountCustomerWillingToPay().compareTo(new BigDecimal(100)) < 0
      )
      .sorted(
        Comparator
          .comparing(CustomerRequest::getAmountCustomerWillingToPay)
          .reversed()
      )
      .collect(Collectors.toList());

    // Filter by the customers willing to pay more than 100 and sort them.
    List<CustomerRequest> cRsForCustomersWillingToPayMoreThanOrEqual100Euros = customerRequests
      .stream()
      .filter(
        c ->
          c.getAmountCustomerWillingToPay().compareTo(new BigDecimal(100)) >= 0
      )
      .sorted(
        Comparator
          .comparing(CustomerRequest::getAmountCustomerWillingToPay)
          .reversed()
      )
      .collect(Collectors.toList());

    // We will work with the deque data structure for both entries.
    Deque<CustomerRequest> cRsForCustomersWillingToPayLessThan100EurosDeque = new ArrayDeque<>(
      cRsForCustomersWillingToPayLessThan100Euros
    );
    Deque<CustomerRequest> cRsForCustomersWillingToPayMoreThan100EurosDeque = new ArrayDeque<>(
      cRsForCustomersWillingToPayMoreThanOrEqual100Euros
    );

    int numberOfEconomyRooms = hotelOccupancy.getNumberOfEconomyRooms();
    int numberOfPremiumRooms = hotelOccupancy.getNumberOfPremiumRooms();

    HotelOccupancyRoomReservationSummaryDTO economyHotelOccupancyRoomReservationSummaryDTO = new HotelOccupancyRoomReservationSummaryDTO(
      "economy"
    );
    HotelOccupancyRoomReservationSummaryDTO premiumHotelOccupancyRoomReservationSummaryDTO = new HotelOccupancyRoomReservationSummaryDTO(
      "premium"
    );

    // Let's fill the premium hotel rooms first.
    while (
      numberOfPremiumRooms > 0 &&
      !cRsForCustomersWillingToPayMoreThan100EurosDeque.isEmpty()
    ) {
      // Fill first the premium rooms with the customers willing to pay more than 100 euros.
      CustomerRequest customerRequest = cRsForCustomersWillingToPayMoreThan100EurosDeque.pop();
      premiumHotelOccupancyRoomReservationSummaryDTO.incrementNumberOfRoomsReservedBy1();
      premiumHotelOccupancyRoomReservationSummaryDTO.addRoomReservationCostToTheBudget(
        customerRequest.getAmountCustomerWillingToPay()
      );
      numberOfPremiumRooms -= 1;
    }

    // The customers that may be upgraded.
    Deque<CustomerRequest> customerRequestsThatMaybeUpgraded = new ArrayDeque<>();

    // Then, fill the economy rooms with the customers willing to pay less than.
    while (
      numberOfEconomyRooms > 0 &&
      !cRsForCustomersWillingToPayLessThan100EurosDeque.isEmpty()
    ) {
      // Of course, we won't force the customers to pay more than 100 euros for non premium rooms.
      CustomerRequest customerRequest = cRsForCustomersWillingToPayLessThan100EurosDeque.pop();
      economyHotelOccupancyRoomReservationSummaryDTO.incrementNumberOfRoomsReservedBy1();
      economyHotelOccupancyRoomReservationSummaryDTO.addRoomReservationCostToTheBudget(
        customerRequest.getAmountCustomerWillingToPay()
      );

      if (upgradable.equals(true)) {
        customerRequestsThatMaybeUpgraded.add(customerRequest);
      }

      numberOfEconomyRooms -= 1;
    }

    // Upgrade the customer requests that could be upgraded.
    while (
      numberOfPremiumRooms > 0 && !customerRequestsThatMaybeUpgraded.isEmpty()
    ) {
      CustomerRequest customerRequest = customerRequestsThatMaybeUpgraded.pop();
      premiumHotelOccupancyRoomReservationSummaryDTO.incrementNumberOfRoomsReservedBy1();
      premiumHotelOccupancyRoomReservationSummaryDTO.addRoomReservationCostToTheBudget(
        customerRequest.getAmountCustomerWillingToPay()
      );
      economyHotelOccupancyRoomReservationSummaryDTO.decrementOfRoomsReservedBy1();
      economyHotelOccupancyRoomReservationSummaryDTO.subtractRoomReservationCostToTheBudget(
        customerRequest.getAmountCustomerWillingToPay()
      );

      numberOfPremiumRooms -= 1;
      numberOfEconomyRooms += 1;
    }

    // Fill again the reset of the empty economy rooms as there's a possibility that some customer requests were upgraded.
    while (
      numberOfEconomyRooms > 0 &&
      !cRsForCustomersWillingToPayLessThan100EurosDeque.isEmpty()
    ) {
      CustomerRequest customerRequest = cRsForCustomersWillingToPayLessThan100EurosDeque.pop();
      economyHotelOccupancyRoomReservationSummaryDTO.incrementNumberOfRoomsReservedBy1();
      economyHotelOccupancyRoomReservationSummaryDTO.addRoomReservationCostToTheBudget(
        customerRequest.getAmountCustomerWillingToPay()
      );
      numberOfEconomyRooms -= 1;
    }

    // In case, it's not upgradable and there are some remaining rooms. Assign them to the remaining customers willing to pay less than
    // 100 euros.
    while (
      numberOfPremiumRooms > 0 &&
      !cRsForCustomersWillingToPayLessThan100EurosDeque.isEmpty()
    ) {
      CustomerRequest customerRequest = cRsForCustomersWillingToPayLessThan100EurosDeque.pop();
      premiumHotelOccupancyRoomReservationSummaryDTO.incrementNumberOfRoomsReservedBy1();
      premiumHotelOccupancyRoomReservationSummaryDTO.addRoomReservationCostToTheBudget(
        customerRequest.getAmountCustomerWillingToPay()
      );
      numberOfPremiumRooms -= 1;
    }

    hotelOccupancyRoomReservationSummaryDTOList.add(
      premiumHotelOccupancyRoomReservationSummaryDTO
    );
    hotelOccupancyRoomReservationSummaryDTOList.add(
      economyHotelOccupancyRoomReservationSummaryDTO
    );

    return hotelOccupancyRoomReservationSummaryDTOList;
  }
}
