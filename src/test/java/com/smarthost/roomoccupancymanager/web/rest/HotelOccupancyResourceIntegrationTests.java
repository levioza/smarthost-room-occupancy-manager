/*
 * Copyright (c) 2021 Fadi William Ghali Abdelmessih<fadi.william.ghali@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies
 * or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

package com.smarthost.roomoccupancymanager.web.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.smarthost.roomoccupancymanager.RoomOccupancyManagerApplication;
import com.smarthost.roomoccupancymanager.domain.Hotel;
import com.smarthost.roomoccupancymanager.domain.HotelOccupancy;
import com.smarthost.roomoccupancymanager.repository.HotelOccupancyRepository;
import com.smarthost.roomoccupancymanager.service.dto.HotelOccupancyDTO;
import com.smarthost.roomoccupancymanager.service.mapper.HotelOccupancyMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link HotelOccupancyResource} REST controller.
 *
 * @author Fadi William Ghali Abdelmessih
 */
@SpringBootTest(classes = RoomOccupancyManagerApplication.class)
@AutoConfigureMockMvc
class HotelOccupancyResourceIntegrationTests {

  private static final LocalDate DATE = LocalDate.now(ZoneId.systemDefault());

  private static final Long HOTEL_ID = 1L;

  private static final Integer DEFAULT_NUMBER_OF_PREMIUM_ROOMS = 3;
  private static final Integer UPDATED_NUMBER_OF_PREMIUM_ROOMS = 5;

  private static final Integer DEFAULT_NUMBER_OF_ECONOMY_ROOMS = 4;
  private static final Integer UPDATED_NUMBER_OF_ECONOMY_ROOMS = 7;

  @Autowired
  private HotelOccupancyRepository hotelOccupancyRepository;

  @Autowired
  private HotelOccupancyMapper hotelOccupancyMapper;

  @Autowired
  private EntityManager em;

  @Autowired
  private ObjectMapper objectMapper;

  @Autowired
  private MockMvc restHotelOccupancyMockMvc;

  private HotelOccupancy hotelOccupancy;

  /**
   * Create an entity for this test.
   *
   * @param em the em
   * @return the hotel occupancy
   */
  public static HotelOccupancy createEntity(EntityManager em) {
    HotelOccupancy hotelOccupancy = new HotelOccupancy()
      .date(DATE)
      .hotel(em.find(Hotel.class, HOTEL_ID))
      .numberOfPremiumRooms(DEFAULT_NUMBER_OF_PREMIUM_ROOMS)
      .numberOfEconomyRooms(DEFAULT_NUMBER_OF_ECONOMY_ROOMS);
    return hotelOccupancy;
  }

  /**
   * Init test.
   */
  @BeforeEach
  public void initTest() {
    hotelOccupancy = createEntity(em);
  }

  /**
   * Create hotel occupancy.
   *
   * @throws Exception the exception
   */
  @Test
  @Transactional
  void createHotelOccupancy() throws Exception {
    int databaseSizeBeforeCreate = hotelOccupancyRepository.findAll().size();

    // Create the HotelOccupancy.
    HotelOccupancyDTO hotelOccupancyDTO = hotelOccupancyMapper.toDto(
      hotelOccupancy
    );
    restHotelOccupancyMockMvc
      .perform(
        post("/api/hotel-occupancies")
          .contentType(MediaType.APPLICATION_JSON)
          .content(TestUtil.convertObjectToJsonBytes(hotelOccupancyDTO))
      )
      .andExpect(status().isOk());

    // Validate the HotelOccupancy in the database.
    List<HotelOccupancy> hotelOccupancyList = hotelOccupancyRepository.findAll();
    assertThat(hotelOccupancyList).hasSize(databaseSizeBeforeCreate + 1);
    HotelOccupancy testHotelOccupancy = hotelOccupancyList.get(
      hotelOccupancyList.size() - 1
    );
    assertThat(testHotelOccupancy.getDate()).isEqualTo(DATE);
    assertThat(testHotelOccupancy.getHotel().getId()).isEqualTo(HOTEL_ID);
    assertThat(testHotelOccupancy.getNumberOfPremiumRooms())
      .isEqualTo(DEFAULT_NUMBER_OF_PREMIUM_ROOMS);
    assertThat(testHotelOccupancy.getNumberOfEconomyRooms())
      .isEqualTo(DEFAULT_NUMBER_OF_ECONOMY_ROOMS);
  }

  /**
   * Check date is required.
   *
   * @throws Exception the exception
   */
  @Test
  @Transactional
  void checkDateIsRequired() throws Exception {
    int databaseSizeBeforeTest = hotelOccupancyRepository.findAll().size();

    // Set the field to null.
    hotelOccupancy.setDate(null);

    // Create the HotelOccupancy, which fails.
    HotelOccupancyDTO hotelOccupancyDTO = hotelOccupancyMapper.toDto(
      hotelOccupancy
    );

    restHotelOccupancyMockMvc
      .perform(
        post("/api/hotel-occupancies")
          .contentType(MediaType.APPLICATION_JSON)
          .content(TestUtil.convertObjectToJsonBytes(hotelOccupancyDTO))
      )
      .andExpect(status().isBadRequest());

    List<HotelOccupancy> hotelOccupancyList = hotelOccupancyRepository.findAll();
    assertThat(hotelOccupancyList).hasSize(databaseSizeBeforeTest);
  }

  /**
   * Check number of premium rooms is required.
   *
   * @throws Exception the exception
   */
  @Test
  @Transactional
  void checkNumberOfPremiumRoomsIsRequired() throws Exception {
    int databaseSizeBeforeTest = hotelOccupancyRepository.findAll().size();

    // Set the field to null.
    hotelOccupancy.setNumberOfPremiumRooms(null);

    // Create the HotelOccupancy, which fails.
    HotelOccupancyDTO hotelOccupancyDTO = hotelOccupancyMapper.toDto(
      hotelOccupancy
    );

    restHotelOccupancyMockMvc
      .perform(
        post("/api/hotel-occupancies")
          .contentType(MediaType.APPLICATION_JSON)
          .content(TestUtil.convertObjectToJsonBytes(hotelOccupancyDTO))
      )
      .andExpect(status().isBadRequest());

    List<HotelOccupancy> hotelOccupancyList = hotelOccupancyRepository.findAll();
    assertThat(hotelOccupancyList).hasSize(databaseSizeBeforeTest);
  }

  /**
   * Check number of economy rooms is required.
   *
   * @throws Exception the exception
   */
  @Test
  @Transactional
  void checkNumberOfEconomyRoomsIsRequired() throws Exception {
    int databaseSizeBeforeTest = hotelOccupancyRepository.findAll().size();

    // Set the field to null.
    hotelOccupancy.setNumberOfEconomyRooms(null);

    // Create the HotelOccupancy, which fails.
    HotelOccupancyDTO hotelOccupancyDTO = hotelOccupancyMapper.toDto(
      hotelOccupancy
    );

    restHotelOccupancyMockMvc
      .perform(
        post("/api/hotel-occupancies")
          .contentType(MediaType.APPLICATION_JSON)
          .content(TestUtil.convertObjectToJsonBytes(hotelOccupancyDTO))
      )
      .andExpect(status().isBadRequest());

    List<HotelOccupancy> hotelOccupancyList = hotelOccupancyRepository.findAll();
    assertThat(hotelOccupancyList).hasSize(databaseSizeBeforeTest);
  }

  /**
   * Check hotel id is required.
   *
   * @throws Exception the exception
   */
  @Test
  @Transactional
  void checkHotelIdIsRequired() throws Exception {
    int databaseSizeBeforeTest = hotelOccupancyRepository.findAll().size();

    // Set the field to null.
    hotelOccupancy.setHotel(null);

    // Create the HotelOccupancy, which fails.
    HotelOccupancyDTO hotelOccupancyDTO = hotelOccupancyMapper.toDto(
      hotelOccupancy
    );

    restHotelOccupancyMockMvc
      .perform(
        post("/api/hotel-occupancies")
          .contentType(MediaType.APPLICATION_JSON)
          .content(TestUtil.convertObjectToJsonBytes(hotelOccupancyDTO))
      )
      .andExpect(status().isBadRequest());

    List<HotelOccupancy> hotelOccupancyList = hotelOccupancyRepository.findAll();
    assertThat(hotelOccupancyList).hasSize(databaseSizeBeforeTest);
  }

  /**
   * Gets all hotel occupancies.
   *
   * @throws Exception the exception
   */
  @Test
  @Transactional
  void getAllHotelOccupancies() throws Exception {
    // Initialize the database.
    hotelOccupancyRepository.saveAndFlush(hotelOccupancy);

    // Get all the hotelOccupancyList.
    MvcResult result = restHotelOccupancyMockMvc
      .perform(get("/api/hotel-occupancies/1?sort=id,desc"))
      .andExpect(status().isOk())
      .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
      .andReturn();

    HotelOccupancyDTO[] hotelOccupancyDTOArray = objectMapper.readValue(
      result.getResponse().getContentAsString(),
      HotelOccupancyDTO[].class
    );
    assertThat(hotelOccupancyDTOArray[0].getHotelId()).isEqualTo(HOTEL_ID);

    Set<Long> hotelIdSet = new HashSet<>();

    Arrays
      .asList(hotelOccupancyDTOArray)
      .forEach(
        hotelOccupancyDTO -> {
          hotelIdSet.add(hotelOccupancyDTO.getHotelId());
        }
      );

    assertThat(hotelIdSet.size()).isEqualTo(1);
  }

  /**
   * Gets hotel occupancy.
   *
   * @throws Exception the exception
   */
  @Test
  @Transactional
  void getHotelOccupancy() throws Exception {
    // Initialize the database.
    hotelOccupancyRepository.saveAndFlush(hotelOccupancy);

    // Get the hotelOccupancy.
    restHotelOccupancyMockMvc
      .perform(
        get(
          "/api/hotel-occupancies/{date}/{hotelId}",
          hotelOccupancy.getDate(),
          hotelOccupancy.getHotel().getId()
        )
      )
      .andExpect(status().isOk())
      .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
      .andExpect(
        jsonPath("$.numberOfPremiumRooms")
          .value(DEFAULT_NUMBER_OF_PREMIUM_ROOMS)
      )
      .andExpect(
        jsonPath("$.numberOfEconomyRooms")
          .value(DEFAULT_NUMBER_OF_ECONOMY_ROOMS)
      );
  }

  /**
   * Gets non existing hotel occupancy.
   *
   * @throws Exception the exception
   */
  @Test
  @Transactional
  void getNonExistingHotelOccupancy() throws Exception {
    // Get the hotelOccupancy that doesn't exist.
    restHotelOccupancyMockMvc
      .perform(get("/api/hotel-occupancies/{date}/{hotelId}", DATE, 100000))
      .andExpect(status().isNotFound());
  }

  /**
   * Create or update hotel occupancy with existing properties.
   *
   * @throws Exception the exception
   */
  @Test
  @Transactional
  void createOrUpdateHotelOccupancyWithExistingProperties() throws Exception {
    // Save the hotel occupancy.
    hotelOccupancyRepository.saveAndFlush(hotelOccupancy);

    int databaseSizeBeforeCreate = hotelOccupancyRepository.findAll().size();

    // Disconnect from session so that the updates on hotelOccupancy are not directly saved in db.
    em.detach(hotelOccupancy);

    // Update the hotel occupancy.
    hotelOccupancy.setId(null);
    hotelOccupancy.setNumberOfEconomyRooms(UPDATED_NUMBER_OF_ECONOMY_ROOMS);
    hotelOccupancy.setNumberOfPremiumRooms(UPDATED_NUMBER_OF_PREMIUM_ROOMS);

    HotelOccupancyDTO hotelOccupancyDTO = hotelOccupancyMapper.toDto(
      hotelOccupancy
    );

    // An entity with existing properties will be upserted.
    restHotelOccupancyMockMvc
      .perform(
        post("/api/hotel-occupancies")
          .contentType(MediaType.APPLICATION_JSON)
          .content(TestUtil.convertObjectToJsonBytes(hotelOccupancyDTO))
      )
      .andExpect(status().isOk());

    // Validate the number of hotel occupancies in the database didn't change.
    List<HotelOccupancy> hotelOccupancyList = hotelOccupancyRepository.findAll();
    assertThat(hotelOccupancyList).hasSize(databaseSizeBeforeCreate);

    // Make sure the record has been updated in the database.
    HotelOccupancy testHotelOccupancy = hotelOccupancyRepository
      .findOneByDateAndHotel_id(DATE, HOTEL_ID)
      .get();
    assertThat(testHotelOccupancy.getNumberOfEconomyRooms())
      .isEqualTo(UPDATED_NUMBER_OF_ECONOMY_ROOMS);
    assertThat(testHotelOccupancy.getNumberOfPremiumRooms())
      .isEqualTo(UPDATED_NUMBER_OF_PREMIUM_ROOMS);
  }

  /**
   * Delete hotel occupancy.
   *
   * @throws Exception the exception
   */
  @Test
  @Transactional
  void deleteHotelOccupancy() throws Exception {
    // Initialize the database.
    hotelOccupancyRepository.saveAndFlush(hotelOccupancy);

    int databaseSizeBeforeDelete = hotelOccupancyRepository.findAll().size();

    // Delete the hotelOccupancy.
    restHotelOccupancyMockMvc
      .perform(
        delete(
          "/api/hotel-occupancies/{date}/{hotelId}",
          hotelOccupancy.getDate(),
          hotelOccupancy.getHotel().getId()
        )
          .accept(MediaType.APPLICATION_JSON)
      )
      .andExpect(status().isNoContent());

    // Validate that the database contains one less item.
    List<HotelOccupancy> hotelOccupancyList = hotelOccupancyRepository.findAll();
    assertThat(hotelOccupancyList).hasSize(databaseSizeBeforeDelete - 1);
  }
}
